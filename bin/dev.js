/* eslint-disable no-console */
/* eslint-disable import/no-unresolved */
import path from 'path'
import Express from 'express'
import httpProxy from 'http-proxy'
import webpack from 'webpack'
import serveStatic from 'serve-static'
import devMiddleware from 'webpack-dev-middleware'
import hotMiddleware from 'webpack-hot-middleware'
import * as config from '../config/webpack/dev'

const app = new Express()
const compiler = webpack([config])
const apiProxy = httpProxy.createProxyServer();

app.use(devMiddleware(compiler, { noInfo: true }))
app.use(hotMiddleware(compiler))

app.use(serveStatic(path.resolve(__dirname, '../public')))

app.use("/api/*", function(req, res) {
  req.url = req.baseUrl;
  apiProxy.web(req, res, {
    target: 'http://62.205.160.6:15080/',
    headers: { 'Access-Control-Allow-Origin': '*' }
  });
});

app.listen(3030, error => {
  if (error) {
    throw error
  }

  console.info('Development server listening on port %s', 3030)
})
