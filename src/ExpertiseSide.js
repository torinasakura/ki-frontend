import React, { PropTypes } from 'react'
import { Paper, Tabs, Tab, TextField } from 'material-ui'

const ExpertiseSide = React.createClass({
  render () {
    return (
      <div
        style={{width:'40rem', height:'100%', position:'fixed', top: '7rem', right:0}}>
        <Tabs tabItemContainerStyle={{background:'transparent', borderBottom:'1px solid #ededed'}}>
          <Tab label='А' style={{color:'green'}}>
            <TextField
              name        = 'comment'
              fullWidth
              multiLine
              value       = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sapien elit, blandit ut est id, tincidunt vestibulum dolor. Mauris eget mollis metus, ut rutrum augue. Suspendisse faucibus rhoncus ante vel ornare. Nulla augue sem, maximus sit amet ligula quis, aliquet volutpat felis. In sed metus quis quam iaculis imperdiet. Fusce non volutpat metus. Vestibulum vestibulum, elit molestie dignissim lobortis, ex augue suscipit risus, convallis ultrices eros mi et justo. Sed interdum ipsum id porta porttitor. Donec bibendum urna eu gravida aliquet. Fusce mi lectus, fermentum nec consectetur sed, tempor a sapien. Quisque eros ipsum, aliquam quis dignissim a, feugiat ac arcu. Aenean vitae pretium enim. Sed aliquam ante et ligula vehicula fermentum. Nunc dictum libero mi, sed scelerisque dui gravida at. Pellentesque sodales, est nec cursus mattis, libero turpis lobortis lorem, eget fermentum lorem arcu quis neque.'
              rows        = {2}
              placeholder = 'Ввести текст'/>
          </Tab>
          <Tab label='Б' style={{color:'red'}}>
            <TextField
              name        = 'comment'
              fullWidth
              placeholder = 'Экспертиза по категории Б'
              value       = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sapien elit, blandit ut est id, tincidunt vestibulum dolor. Mauris eget mollis metus, ut rutrum augue. Suspendisse faucibus rhoncus ante vel ornare. Nulla augue sem, maximus sit amet ligula quis, aliquet volutpat felis. In sed metus quis quam iaculis imperdiet. Fusce non volutpat metus. Vestibulum vestibulum, elit molestie dignissim lobortis, ex augue suscipit risus, convallis ultrices eros mi et justo. Sed interdum ipsum id porta porttitor. Donec bibendum urna eu gravida aliquet. Fusce mi lectus, fermentum nec consectetur sed, tempor a sapien. Quisque eros ipsum, aliquam quis dignissim a, feugiat ac arcu. Aenean vitae pretium enim. Sed aliquam ante et ligula vehicula fermentum. Nunc dictum libero mi, sed scelerisque dui gravida at. Pellentesque sodales, est nec cursus mattis, libero turpis lobortis lorem, eget fermentum lorem arcu quis neque.'
              multiLine
              rows        = {2}/>
          </Tab>
          <Tab label='В' style={{color:'black'}}>
            <TextField
              name='comment'
              fullWidth
              multiLine
              rows={2}
              placeholder='Ввести текст В'/>
          </Tab>
          <Tab label='Г' style={{color:'black'}}>
            <TextField
              name='comment'
              fullWidth
              multiLine
              rows={2}
              placeholder='Ввести текст В'/>
          </Tab>
          <Tab label='Д' style={{color:'black'}}>
            <TextField
              name='comment'
              fullWidth
              multiLine
              rows={2}
              placeholder='Ввести текст В'/>
          </Tab>
        </Tabs>
      </div>
    )
  }
})

export default ExpertiseSide
