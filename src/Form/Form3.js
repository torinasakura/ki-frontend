import React, { PropTypes } from 'react'
import { isEmpty, concat, forEach, merge, map, clone, isFunction, filter } from 'lodash'
import Text from './Fields/Text'
import Select from './Fields/Select'
import MyDate from './Fields/Date'
import MyArray from './Fields/Array'
import File from './Fields/File'

const components = {
  'text':   Text,
  'select': Select,
  'date':   MyDate,
  'toggle': Text,
  'file':   File,
  'array':  MyArray
}


const findFunctions = (data, ignore) => {
  let valsToFuncs = {}
  let fBlocks     = []
  forEach(data, (block, index) => {
    forEach(block, (value, key) => {
      if (isFunction(value) && ignore.indexOf(key) > -1 === false) {
        const arg        = value.toString().match(/\((.+)\)/)[1]
        const usedValues = value.toString().match(/values(?:\.|(?:\[\'))(.+?)(?:(?:\'\])|\b)/gi)
        forEach(usedValues, usedValue =>
          valsToFuncs[usedValue] = concat(valsToFuncs[usedValue], {index, value})
        )
        fBlocks = concat(fBlocks, {index, value})
        }
      })
    })
  return fBlocks
}

const reCompute = (value) =>
  valsToFuncs
    ? valsToFuncs[value]
      ? compute(value)
      : null//update()
    : null//update()

const Block = React.createClass({

  setValue(value) {
    this.props.setValue(this.props.block.name, value)
  },

  render() {
    let block       = clone(this.props.block)
    block           = this.props.block.label ? block : merge({}, block, {label: block.name})
    const Component = components[block.type]
    const show      = block.show === undefined
                        ? true
                        : block.show
    return (
      <div className = 'block'>
      {show
        ? <Component
            {...block}
            setValue = {this.setValue}
            values   = {this.props.values}/>
        : null}
      </div>
    )
  }
})

export default React.createClass({

  propTypes:{
    form:     React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    returns:  React.PropTypes.func.isRequired,
    values:   React.PropTypes.oneOfType([
      React.PropTypes.array,
      React.PropTypes.object
    ]),
  },

  getDefaultProps() {
    return {
      values:   {},
      moreData: {},
      ignore:   ['options', 'optionLabel']
    }
  },

  getInitialState() {
    return {
      form:    [],
      values:  clone(this.props.values),
      fBlocks: findFunctions(this.props.form, this.props.ignore),
      loads: true
    }
  },

  componentWillMount() {
    this.computeForm(this.props.form, this.props.values)
  },

  computeForm(nextForm, nextValues) {
    const fBlocks      = this.state.fBlocks
    const values       = clone(nextValues)
    const computedForm = map(nextForm, block => {
      if(nextValues[block.name])
        block.value = nextValues[block.name]
      return block
    })

    const update = (i, key, value) =>
      computedForm[i][key] = value

    const compute = (value, callback) => {
      const cFunc = value(values)
      cFunc ?
        cFunc.then
          ? cFunc.then((result) => callback(result))
          : callback(cFunc)
      : callback(null)
    }

    const set = (acc, computedValue) => {
      const key  = fBlocks[acc].value.name
      const next = acc + 1

      update(fBlocks[acc].index, key, computedValue)

      const currySet = (index) =>
        (value) => set(index, value)

      if (next < fBlocks.length) {
        if (key === 'show' && computedValue == null) {
          computedForm[fBlocks[acc].index].value = null
          fBlocks[acc]
          compute(fBlocks[next].value, currySet(next))
        }
        if (key === 'value') {
          if(computedValue === values[computedForm[fBlocks[acc].index].name]) {
            compute(fBlocks[next].value, currySet(next))
          } else{
            values[computedForm[fBlocks[acc].index].name] = computedValue
            compute(fBlocks[0].value, currySet(0))
          }
        } else {
          compute(fBlocks[next].value, currySet(next))
        }
      } else {
        if (key !== 'show')
          values[computedForm[fBlocks[acc].index].name] = computedValue
        this.updateState(computedForm, values)
      }
    }

    const start = (value) => set(0, value)

    isEmpty(fBlocks) === false
      ? compute(fBlocks[0].value, start)
      : this.updateState(computedForm, values)
  },

  updateState(form, values) {
    this.setState({form, values, loads: false})
    this.returns(values)
  },

  returns(values) {
    this.props.returns(values)
  },

  setValue(key, value) {
    let newValues  = this.state.values
    newValues[key] = value
    this.computeForm(this.state.form, newValues)
  },

  render() {
    return <div>
      {map(this.state.form, (block, i) =>
        <Block
          block    = {block}
          setValue = {this.setValue}
          values   = {this.state.values} key={i}/>)}
    </div>
  }
})
