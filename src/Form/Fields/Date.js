import React, { PropTypes } from 'react'
import { DatePicker } from 'material-ui'
import moment from 'moment'
import areIntlLocalesSupported from 'intl-locales-supported'

let DateTimeFormat
if (areIntlLocalesSupported(['ru'])) {
  DateTimeFormat = global.Intl.DateTimeFormat
} else {
  const IntlPolyfill = require('intl')
  DateTimeFormat     = IntlPolyfill.DateTimeFormat
  require('intl/locale-data/jsonp/ru')
}

export default React.createClass({

  returns(event, value) {
    const fixedValue = moment(value).add(3, 'h').toISOString()
    this.props.setValue(fixedValue)
  },

  render() {
    return(
      <DatePicker
        fullWidth
        floatingLabelText = {this.props.label || this.props.name}
        value             = {this.props.value ? new Date(this.props.value) : null}
        locale            = 'ru'
        okLabel           = 'ОК'
        cancelLabel       = 'Отменить'
        DateTimeFormat    = {DateTimeFormat}
        formatDate        = {(date) => moment(date).format('LL')}
        name              = {this.props.name}
        onChange          = {this.returns}
      />
    )
  }
})
