import React, { PropTypes } from 'react'
import { IconButton, TextField, LinearProgress } from 'material-ui'
import FileFileDownload from 'material-ui/svg-icons/file/file-download'
import NavigationClose from 'material-ui/svg-icons/navigation/close'
import { get } from 'lodash'
import axios from 'axios'

export default React.createClass({

  componentWillMount() {
    if (this.props.value)
      axios.get('files/' + this.props.value + '/info')
      .then((response) => this.setState({
        file: {
          name: response.data.file_name}}))
  },

  download() {
    window.open(axios.defaults.baseURL + '/files/' + this.props.value)
  },

  remove() {
    this.props.setValue(null)
    this.setState({
      file: null
    })
  },

  input() {
    if (this.refs.file.files[0]) {
      this.setState({
        file:      this.refs.file.files[0],
        error:     null,
        uploading: true
      })
      const formData = new FormData()
      formData.append('file', this.refs.file.files[0])
      var request = new XMLHttpRequest()
      request.onreadystatechange = () => {
        if (request.status == 201) {
          this.setState({uploading: false})
          this.props.setValue(JSON.parse(request.responseText).uuid)
        } else if (request.status != 201 && request.status != 0) {
          this.setState({
            uploading: false,
            error:     'Ошибка (' + request.statusText + ')'
          })
        }
      }
      request.open("POST", "/api/files")
      request.setRequestHeader('Authorization', 'Token ' + JSON.parse(localStorage.getItem('user')).token)
      request.send(formData)
    } else {
      this.setState({
        uploading: false,
        error:     null
      })
    }
  },

  getInitialState() {
    return {
      uploading: false
    }
  },

  render() {
    const controls = () => {if (this.props.value) {
      return (
      <div style={{position:'absolute', right:'-1rem', bottom:'.5rem'}}>
        <IconButton
          style     = {{width:'3rem', height:'3rem', padding:'.5rem'}}
          iconStyle = {{width:'1.9rem', height:'1.9rem'}}
          onClick   = {this.download}>
          <FileFileDownload color='gray'/>
        </IconButton>
        <IconButton
          style     = {{width:'3rem', height:'3rem', padding:'.5rem'}}
          iconStyle = {{width:'2rem', height:'2rem'}}
          onClick   = {this.remove}>
          <NavigationClose color='gray'/>
        </IconButton>
      </div>
    )
  }}
    return (
      <div style={{position:'relative'}}>
        <TextField
          errorText         = {this.state.error}
          name              = {this.props.name}
          floatingLabelText = {this.props.label || this.props.name}
          value             = {get(this.state, 'file.name')}
          fullWidth/>
          {this.state.uploading
            ? <LinearProgress
                style={{position:'absolute', marginTop:'-1rem'}}
                mode='indeterminate'/> : null}
          <input
            type='file' ref='file' onChange={this.input}
            style={{opacity:0, left:0, width: '100%', cursor:'pointer', position:'absolute', bottom:0, top: 0}}/>
          {controls()}
      </div>
    )
  }
})
