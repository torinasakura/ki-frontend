import React, { PropTypes } from 'react'
import { FlatButton } from 'material-ui'
import { map, forEach, clone, isObject, uniqBy, concat, isFunction } from 'lodash'
import Form from '../Form3'

const ArrayItem = (props) => {
  return (
    <div style={props.style} className='array__item'>
      {props.children}
      <div className='array__item__separator' onClick={props.remove}>
        <div className='array__item__separator__remove'>
          <div className='array__item__separator__remove__cross'>+</div>
        </div>
      </div>
    </div>
  )
}


export default React.createClass({

  getDefaultProps() {
    return {
      value: []
    }
  },

  setValue(i, value) {
    let newValues = clone(this.props.value)
    newValues[i]  =
      this.props.object.length === 1
        ? value[this.props.object[0].name]
        : value
  },

  add() {
    let newValues = clone(this.props.value)
    this.props.object.length === 1
      ? newValues = concat(newValues, '')
      : newValues = concat(newValues, {})
    this.props.setValue(newValues)
  },

  remove(i) {
    let newValues = clone(this.props.value)
    newValues.splice(i, 1)
    this.props.setValue(newValues)
  },

  render() {
    return(
      <div className='array'>
        <div className='array__title'>{this.props.label || this.props.name}</div>
        <p>{this.props.description}</p>
          {map(this.props.value, (obj, i) =>
            <ArrayItem key={i} remove={this.remove.bind(null, i)}>
              <Form
                form     = {this.props.object}
                returns  = {this.setValue.bind(this, i)}
                values   = {this.props.object.length > 1 ? obj : {[this.props.object[0].name]: obj}}
                 />
            </ArrayItem>)}
      {this.props.addable
        ? <FlatButton
            primary
            onTouchTap = {this.add}
            style      = {{marginLeft:'-2rem'}}
            label      = 'Добавить'/>
        : null}
    </div>
    )
  }
})
