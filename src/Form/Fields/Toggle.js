import React, { PropTypes } from 'react'

const selected = {
  padding:    '.5rem 1.5rem',
  color:      'rgba(0,0,0,.84)',
  background: 'white',
}
const notSelected = {
  color:      'rgba(0,0,0,.56)',
  padding:    '.5rem 1.5rem',
  background: 'rgba(0,0,0,.04)',
  boxShadow:  'inset 0px 0px 8px rgba(0,0,0,.08)',
}

const style = (value, me) =>
  value === me
    ? selected
    : notSelected

export default React.createClass({

  setValue(e) {
    const value =
      this.props.value === undefined
        ? 'Да'
        : this.props.value === 'Да'
          ? 'Нет'
          : 'Да'
    this.props.setValue(value)
  },

  render() {
    const color = (current) =>
      this.props.value
        ? this.props.value === current
          ? 'white'
          : 'rgba(0,0,0,.05)'
        : 'rgba(0,0,0,.05)'

    return (
      <div
        className  = 'toggle'
        onClick    = {this.setValue}
        style      = {{display:'flex', alignItems:'center', margin:'2rem 0'}}>
        <div style = {{flexGrow:'1'}}>
        {this.props.label || this.props.name}
        </div>
        <div style = {{display:'flex', border:'1px solid lightgray', borderRadius: '2px'}}>
          <div style = {style(this.props.value, 'Нет')}>Нет</div>
          <div style = {style(this.props.value, 'Да')}>Да</div>
        </div>
      </div>
    )
  }
})
