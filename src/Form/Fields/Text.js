import React, { PropTypes } from 'react'
import { TextField } from 'material-ui'

export default React.createClass({

  getInitialState() {
    return({
      value:     this.props.value,
      timeStamp: 0
    })
  },

  componentWillReceiveProps(nextProps) {
    this.setState({value: nextProps.value})
  },

  change(e) {
    const value     = e.target.value
    const timeStamp = e.timeStamp
    this.setState({value, timeStamp})
    window.setTimeout(this.setValue, 800)
  },

  setValue(e) {
      this.props.setValue(e.target.value)
  },

  render() {
    return (
      <TextField
        style             = {{overflow:'hidden', display:'block'}}
        hintText          = {this.props.hintText}
        floatingLabelText = {this.props.label || this.props.name}
        name              = {this.props.name}
        value             = {this.props.value}
        onChange          = {this.setValue}
        errorText         = {this.props.errorText}
        disabled          = {this.props.disabled}
        multiLine         = {this.props.multiline}
        type              = {this.props.format}
        fullWidth />
    )
  }
})
