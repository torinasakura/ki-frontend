import React, { PropTypes } from 'react'
import { SelectField, ListItem, TextField, Paper, AutoComplete } from 'material-ui'
import { map, isObject, uniqBy, concat, isFunction } from 'lodash'
import onClickOutside from 'react-onclickoutside'

export default onClickOutside(React.createClass({

  getInitialState() {
    return {
      options: [],
      value:   null,
    }
  },

  handleClickOutside(evt) {
    this.close()
  },

  getOptions() {
    const set = (options) => this.setState({
      options,
      opened:   true,
      width:    this.refs.input.offsetWidth,
      y:        this.refs.input.getBoundingClientRect().bottom,
    })

    isFunction(this.props.options)
      ? this.props.options(this.props.values || {}, this.props.moreData || {}).then
        ? this.props.options(this.props.values || {}, this.props.moreData || {}).then((result) => set(result))
        : set(this.props.options(this.props.values || {}, this.props.moreData || {}))
      : set(this.props.options)
  },

  close() {
    this.setState({
      options: [],
      opened:  false,
    })
  },

  handleChange(value, i) {
    this.props.setValue(value)
    this.close()
  },

  render() {
    return (
      <div ref='input' style={{position: 'relative'}}>
             <TextField
               ref               = 'input'
               onFocus           = {this.getOptions}
               name              = {this.props.name}
               floatingLabelText = {this.props.label || this.props.name}
               value             = {this.props.optionLabel && this.props.value
                                     ? this.props.optionLabel(this.props.value)
                                     : this.props.value}
               disabled = {this.props.disabled}
               multiLine
               fullWidth />
             <Paper
               disableOnClickOutside = {true}
               style                 = {{
                position:   'absolute',
                zIndex:     2100,
                right:      0,
                left:       0,
                opacity:    this.state.opened ? 1 : 0,
                marginTop:  '-1rem',
                maxHeight:  this.state.opened ? `calc(100vh - ${this.state.y}px)` : 0,
                transition: 'max-height .3s ease-out, opacity .3 ease-out',
                overflowY:  'auto'}}>
               {this.state.options
                 ? map(this.props.value
                   ? uniqBy(concat(this.state.options, this.props.value))
                   : this.state.options, (option, i) =>
                       <ListItem
                         onTouchTap  = {this.handleChange.bind(null, option)}
                         value       = {option}
                         style       = {{lineHeight: '3rem'}}
                         primaryText = {<div>{this.props.optionLabel ? this.props.optionLabel(option) : option}</div>}
                         label       = {<div>{this.props.optionLabel ? this.props.optionLabel(option) : option}</div>}
                         key         = {i}/>)
                 : 'Нет данных'}
               </Paper>
           </div>
    )
  }
})
)
