import React from 'react'
import { render } from 'react-dom'
import injectTapEventPlugin from 'react-tap-event-plugin'
import 'flex-layouts/lib/flex-layouts.css'
import moment from 'moment'
import axios from 'axios'
import configureStore from './store/configureStore'
import Root from './containers/Root'

injectTapEventPlugin()
moment.locale('RU')

axios.defaults.baseURL                      = '/api'
axios.defaults.headers.post['Content-Type'] = 'application/json'

const store = configureStore()

let token = undefined
store.subscribe(() => {
  if (store.getState().user.token !== token) {
    token = store.getState().user.token
    axios.defaults.headers.common['Authorization'] = 'Token ' + token
  }
})

render(
  <Root store={store} />,
  document.getElementById('root')
)

if (module.hot) {
  module.hot.accept('./containers/Root', () => {
    const HotRoot = require('./containers/Root').default // eslint-disable-line global-require

    render(
      <HotRoot store={store} />,
      document.getElementById('root')
    )
  })
}
