import React, { PropTypes } from 'react'
import { map, isNaN, isBoolean, forEach, find } from 'lodash'
import update from 'react-addons-update'
import { TextField, Checkbox, SelectField, MenuItem, FlatButton } from 'material-ui'
import axios from 'axios'

const errors = (validations, value, form) =>
  map(validations, (validation) =>
    validation.validation(value, form) ? validation.message : null)

const SelectableObject = React.createClass({
  getData() {
    axios.get(this.props.dictionary)
    .then((response) => this.setState({
      options: response.data
    }))
  },
  getInitialState() {
    return {
      options: null,
      value: this.props.value
    }
  },
  returnValue(e, i, value){
    let myObj = {}
    forEach(this.props.returns, (field, key) => myObj[key] = value[field])
    this.props.handleChange({target: {value: myObj}})
  },
  render () {
    return (
      <div>
        <SelectField floatingLabelText={this.props.label} onClick={this.getData} onChange={this.returnValue}>
          {map(this.state.options, (option) => <MenuItem value={option} key={option.name} primaryText={option.name} />)}
        </SelectField>
        <div>
          {this.props.value ? map(this.props.value, (value, key) =>
            <div className='flexed' key={key}>
              <div>{key}</div>
              <div>{value}</div>
            </div>) : "Выберите значение"}
        </div>
      </div>
    )
  }
})


const ArrayOfObjects = React.createClass({
  returnValues(values, path) {
    let newValues   = this.props.value
    newValues[path] = values
    this.props.handleChange({target:{value:newValues}})
  },
  add() {
    let newObject = {}
    forEach(this.props.object, (object) => newObject[object.name] = null)
    this.returnValues(newObject, this.props.value.length)
  },
  render() {
    return(
      <div>
        <div>{this.props.label}</div>
      {map(this.props.value, (obj, i) =>
      <Form
        style        = {{display:'flex'}}
        data         = {this.props.object}
        values       = {obj}
        returnValues = {this.returnValues}
        i            = {i}
        key          = {i}/>)}
      {this.props.addable ? <FlatButton onClick={this.add} label='Добавить'/> : null}
    </div>
    )
  }
})
const MyToggle = React.createClass({
  toggle(){
    this.props.handleChange({target: {value: !this.props.value}})
  },
  render () {
    return (
      <Checkbox
        style   = {{margin: '3rem 0'}}
        onCheck = {this.toggle}
        label   = {this.props.label}
        checked = {this.props.value}/>
    )
  }
})

const Text = (block) =>
  <TextField
    floatingLabelText = {block.label}
    type              = {block.type}
    value             = {block.value}
    onChange          = {block.handleChange}
    errorText         = {block.error}
    disabled          = {block.disabled}/>

const Select = React.createClass( {
  returnValue(event, index, value){
    this.props.handleChange({target: {value: value}})
  },
  render() {
  return(
  <SelectField
    value             = {this.props.value}
    floatingLabelText = {this.props.label}
    onChange          = {this.returnValue}>
    {map(this.props.options, (option) =>
      <MenuItem
        value         = {option.value}
        key           = {option.value}
        primaryText   = {option.label}/>)}
  </SelectField>)}
})

const components = {
  text:           Text,
  select:         Select,
  password:       Text,
  number:         Text,
  toggle:         MyToggle,
  arrayOfObjects: ArrayOfObjects,
  object:         SelectableObject
}

const FieldBlock = React.createClass({
  propTypes: {
    type: PropTypes.oneOf(map(components, (item, key) => key))
  },
  handleChange(e) {
    let typedValue = e.target.value
    if (typedValue === '') {
      typedValue = null
    } else if (isBoolean(typedValue) !== true && isNaN(Number(typedValue)) === false) {
      typedValue = Number(typedValue)
    }

    this.props.changeValue(
      this.props.i, typedValue)
  },
  componentWillReceiveProps(nextProps) {
    this.setState({
      errors: errors(this.props.validations, nextProps.value, nextProps.form)
    })
  },
  getInitialState() {
    return {
      errors: errors(this.props.validations, this.props.value, this.props.form)
    }
  },
  render () {
    const Component = components[this.props.type]
    return (
      <div>
        <Component {...this.props} handleChange={this.handleChange} errors={this.state.errors}/>
      </div>
    )
  }
})

const Form = React.createClass({
  changeValue(path, value) {
    let nextForm = this.state.form
    nextForm[path].value = value
    let values = {}
    map(nextForm, (item) => values[item.name] = item.value)
    this.props.returnValues(values, this.props.i)
    this.setState({
      form:   nextForm,
      values: values
    })
  },
  componentWillReceiveProps(nextProps){
    this.setState({
      form: nextProps.data
    })
  },
  getInitialState() {
    return {
      form: this.props.data
    }
  },
  render () {
    let form = this.props.values ?
      map(this.state.form, (field) => {
        field.value = this.props.values[field.name]
        return field}) : this.state.form

    form = this.props.errors ? map(this.state.form, (field) => {
      field.error = this.props.errors[field.name]
      return field}) : form
    return (
      <div style={this.props.style}>{map(form, (block, i) =>
          <FieldBlock {...block}
            changeValue = {this.changeValue}
            form        = {form}
            i           = {i}
            key         = {block.name} />)}</div>
    )
  }
})

export default Form
