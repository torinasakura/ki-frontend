import React, { PropTypes } from 'react'
import Menu from './Components/Menu'
import { Tabs, Tab } from 'material-ui'

const Layout = React.createClass({

  contextTypes: {
    location: React.PropTypes.func.isRequired,
    router: React.PropTypes.object.isRequired
  },

  go(value) {
    this.context.router.push(value)
  },

  render () {
    const location = this.context.location().pathname.replace('/', '') === ''
                      ? 'actions'
                      : this.context.location().pathname.replace('/', '')
    return (
      <div style={{height:'100vh', overflow:'hidden'}}>
        <Menu>
          {/*<div className='menu__tabs__tab menu__tabs__tab--active'>Мероприятия</div>*/}
          {/*<div className='menu__tabs__tab'>Планы</div>*/}
          <div style={{width:'38rem'}}>
            <Tabs onChange = {this.go} value={location} >
              <Tab label = 'Мероприятия' value='actions'/>
              <Tab label = 'Планы' value='plans'/>
            </Tabs>
          </div>
        </Menu>
        <div style={{marginTop: this.context.location().pathname == '/login' ? '-2rem' : '6rem', padding:'3rem 0', boxSizing:'border-box', height:'calc(100vh - 6rem)', overflowY:'auto'}}>
          {this.props.children}</div>
      </div>
    )
  }
})

export default Layout
