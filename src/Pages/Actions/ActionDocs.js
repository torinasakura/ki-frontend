import React from 'react'
import { List, ListItem, IconButton } from 'material-ui'
import EditorInsertDriveFile from 'material-ui/svg-icons/editor/insert-drive-file'
import AlertError from 'material-ui/svg-icons/alert/error'
import { map } from 'lodash'

import Interpunct from '../../Components/Interpunct'
import Flex from '../../Components/Flex'
import MyDate from '../../Components/Date'

const ActionDoc = (doc) =>
  <a href={doc['Файл'] ? '/api/files/' + doc['Файл'] : null} target='blank'>
    <ListItem style={{margin:'0 -3rem'}} innerDivStyle={{padding:'1.5rem 3rem'}}>
      <Flex>
        <IconButton
          tooltip   = {doc['Файл'] ? 'Скачать' : 'Документ отсутствует'}
          style     = {{marginLeft: '-15px', marginTop: '-7px'}}
          iconStyle = {{fill:'gray'}}>
          {doc['Файл']
            ? <EditorInsertDriveFile />
            : <AlertError />}
        </IconButton>
        <div style={{fontSize:'14px'}}>
          <div style={{lineHeight:'3rem'}}>
            {doc['Наименование'] || 'Наименование не указано'}
          </div>
          {doc.stub
            ? <Interpunct style={{fontSize:'13px', color:'gray'}}>
                {doc['Тип документа'] || 'Тип не указан'}
                {doc['Статус документа по информатизации'] || 'Статус не указан'}
              </Interpunct>
            : <Interpunct style={{fontSize:'13px', color:'gray'}}>
                {(doc['Тип документа'] || 'Тип не указан') + (doc['Номер'] ? ' №' + doc['Номер'] : ' без номера')}
                {doc['Дата'] ? <MyDate value={doc['Дата']} /> : 'Дата не указана'}
                {doc['Орган государственной власти, принявший документ'] || 'ОГВ не указано'}
              </Interpunct>}
        </div>
      </Flex>
    </ListItem>
  </a>

const ActionDocs = ({name, value, stub}) =>
  value === undefined || value.length === 0
    ? <Flex className='ActionFieldBlock'>
        <div style={{width:'38.2%', flexShrink: 0, marginRight:'2rem'}}>{name || 'Документы'}</div>
        <div style={{color:'gray'}}>Не указано</div>
      </Flex>
    : <div className='ActionFieldBlock'>
        <div style={{paddingBottom:'.5rem'}}>{name || 'Документы'}</div>
        <div>
          {map(value, (doc, i) => <ActionDoc key={i} {...doc} stub={stub}/>)}
        </div>
      </div>



export default ActionDocs
