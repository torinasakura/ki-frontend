import React, { PropTypes } from 'react'
import axios from 'axios'
import { map } from 'lodash'
import { Link } from 'react-router'
import { RowLayout, Layout } from 'flex-layouts'

import Interpunct from '../../Components/Interpunct'

import ListItem from './components/ListItem'
import ContextMenu from './components/ContextMenu'
import MenuItem from './components/MenuItem'

const ActionLI = React.createClass({

  contextTypes: {
    router: React.PropTypes.object.isRequired,
    error:  React.PropTypes.func.isRequired
  },

  go() {
    this.context.router.push('/actions/' + this.props.system.uuid)
  },

  delete() {
    axios.delete('events/' + this.props.system.uuid).then((response) =>
      response.data.error
        ? this.context.error(response.data.error)
        : this.props.update())
  },

  removeFromPlan() {
    axios.delete(`events/${this.props.system.uuid}/year`).then((response) =>
      response.data.error
        ? this.context.error(response.data.error)
        : this.props.update())
  },

  render() {
    console.log(this.props.system['Год'])
    return (
      <div style={{position: 'relative'}}>
        <Link to={`/actions/${this.props.system.uuid}`}>
          <ListItem
            primaryText={this.props['Общие сведения']['Наименование мероприятия'] || this.props['Общие сведения']['Уникальный номер мероприятия']}
            mutedText={<Interpunct>{this.props.system['Статус']}{this.props.system['Департамент']}</Interpunct>}
          />
        </Link>
        <ContextMenu icon={<span>+</span>} position='top-left'>
          <MenuItem clickHandler={this.delete}>Удалить</MenuItem>
          <MenuItem hideIf={this.props.system['Год']} clickHandler={this.removeFromPlan}>Исключить из плана</MenuItem>
        </ContextMenu>
      </div>
    )
  }
})

export default ActionLI
