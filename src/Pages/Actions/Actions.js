import React, { PropTypes } from 'react'
import axios from 'axios'
import { map, concat, omit } from 'lodash'
import { List, Paper, DropDownMenu, ListItem, MenuItem, TextField, IconButton } from 'material-ui'
import moment from 'moment'
import Container from '../../Components/Container'
import Hide from '../../Components/Hide'
import Flex from '../../Components/Flex'
import HumanDates from '../../Components/Date'
import ActionLI from './ActionLI'
import ActionNew from './ActionNew'
import Menu from '../../Components/Menu'
import NavigationClose from 'material-ui/svg-icons/navigation/close'

const CodeText = React.createClass({

  getInitialState() {
    return ({
      value:     '',
      timeStamp: 0
    })
  },

  change(e) {
    const value     = e.target.value
    const timeStamp = e.timeStamp
    this.setState({value, timeStamp})
    window.setTimeout(this.setValue, 600)
  },

  clear() {
    this.setState({value: ''})
    this.props.return('code.like', null, null, '')
  },

  setValue() {
    if (performance.now() - this.state.timeStamp > 600)
      this.props.return('code.like', null, null, this.state.value)
  },

  render() {
    const iconStyle = {
      height:  '2.5rem',
      width:   '2.5rem',
      color:   'gray',
      padding: 0
    }
    return (
      <Flex itemCenter>
        <input
          type        = 'text'
          value       = {this.state.value}
          placeholder = 'Код мероприятия'
          onChange    = {this.change}
          style       = {{
            background:    'transparent',
            border:        0,
            lineHeight:    'auto',
            fontSize:      '13px',
            fontFamily:    'Roboto',
            color:         'gray',
            textTransform: 'uppercase',
            marginLeft:    '2rem',
            width:         '21rem'}}/>
          {this.state.value
            ? <IconButton
                onTouchTap = {this.clear}
                iconStyle  = {iconStyle}
                style      = {iconStyle}>
                <NavigationClose/>
              </IconButton>
            : null}
      </Flex>
    )
  }
})

const Actions = React.createClass({

  contextTypes: {
    router: React.PropTypes.object.isRequired,
    error:  React.PropTypes.func.isRequired,
    user:   React.PropTypes.func.isRequired
  },

  getActions(param, value) {
    let params    = this.state.params
    params.offset = 0
    value !== null
      ? params[param] = value
      : params = omit(params, param)
    axios({
      url:    'events',
      method: 'get',
      params: params
    }).then((response) =>
      this.setState({actions: response.data, params, more: response.data.length >= 20}))
  },

  componentWillMount() {
    this.getActions('status', this.context.user().user.role === 'Куратор МКС' ? 'На экспертизе в МКС' : null)
    axios.get('dics/dictionaries/ogv').then(response =>
      this.setState({
        filters: {
          'code.like': {
            value: null,
          },
          status: {
            value: this.context.user().user.role === 'Куратор МКС' ? 'На экспертизе в МКС' : null,
            items: [{
              value:       null,
              primaryText: 'Все статусы'
            }, {
              value:       'Черновик',
              primaryText: 'Черновик'
            }, {
              value:       'На согласовании в ЦА',
              primaryText: 'На согласовании в ЦА'
            }, {
              value:       'Направлено в ЭБ',
              primaryText: 'Направлено в ЭБ'
            }, {
              value:       'Обработано в ЭБ',
              primaryText: 'Обработано в ЭБ'
            }, {
              value:       'На доработке',
              primaryText: 'На доработке'
            }, {
              value:       'На экспертизе в МКС',
              primaryText: 'На экспертизе в МКС'
            }, {
              value:       'Положительное заключение',
              primaryText: 'Положительное заключение'
            }, {
              value:       'Отрицательное заключение',
              primaryText: 'Отрицательное заключение'
            }, {
              value:       'Проведение/Финансирование (+/+)',
              primaryText: 'Проведение/Финансирование (+/+)'
            }, {
              value:       'Проведение/Финансирование (-/-)',
              primaryText: 'Проведение/Финансирование (-/-)'
            }, {
              value:       'Проведение/Финансирование (+/-)',
              primaryText: 'Проведение/Финансирование (+/-)'
            }, {
              value:       'Проведение/Финансирование (-/+)',
              primaryText: 'Проведение/Финансирование (-/+)'
            }
          ]
          },
          grbs: {
            value: null,
            items: concat(
              [{value: null, primaryText: 'Все ОГВ'}],
              map(response.data, item => ({value: item.grbs, primaryText: item.short_name})))
          }
        }
      })
    )
  },

  getInitialState() {
    return {
      actions: [],
      params: {
        limit:  20,
        offset: 0,
        grbs:   this.context.user().user.grbs,
        dept:   this.context.user().user.role !== 'Куратор ОГВ' && this.context.user().user.dept
                  ? this.context.user().user.dept
                  : null,
      },
      more: true,
    }
  },

  filter(param, event, index, value) {
    let filters = this.state.filters
    filters[param].value = value
    this.setState({
      filters: filters
    })
    this.getActions(param, value)
  },

  more() {
    let params     = this.state.params
    params.offset += 20
    axios({
      url:    'events',
      method: 'get',
      params: params
    })
    .then((response) => this.setState({actions: concat(this.state.actions, response.data), params, more: response.data.length >= 20}))
  },

  render() {
    return (
      <Container>
        {this.state.filters
          ? <Flex itemCenter style = {{marginTop: '-3rem', padding: '1rem 0'}}>
              <DropDownMenu
                style              = {{marginTop:    '-7px', flexShrink: 0}}
                labelStyle         = {{color:        'gray',
                                      textTransform: 'uppercase',
                                      fontSize:      '13px'}}
                underlineStyle     = {{display:      'none'}}
                value              = {this.state.filters.status.value}
                onChange           = {this.filter.bind(this, 'status')}>
                {map(this.state.filters.status.items, item =>
                  <ListItem
                    style          = {{lineHeight: '3rem'}}
                    innerDivStyle  = {{padding: '1rem 3rem' }}
                    key            = {item.value}
                    value          = {item.value}
                    primaryText    = {item.primaryText} />)}
              </DropDownMenu>
              <Hide
                style = {{margin: '-1rem 0'}}
                for   = {['Руководитель департамента', 'Куратор ОГВ']}>
                <DropDownMenu
                  autoWidth          = {false}
                  menuStyle          = {{width: '80rem'}}
                  style              = {{flexShrink: 0, maxWidth: '50rem', overflow: 'hidden'}}
                  labelStyle         = {{color: 'gray', textTransform: 'uppercase', fontSize: '13px'}}
                  underlineStyle     = {{display: 'none'}}
                  value              = {this.state.filters.grbs.value}
                  onChange           = {this.filter.bind(this, 'grbs')}>
                  {map(this.state.filters.grbs.items, item =>
                    <ListItem
                      style          = {{lineHeight: '3rem'}}
                      innerDivStyle  = {{padding: '1rem 3rem' }}
                      key            = {item.value}
                      value          = {item.value}
                      primaryText    = {item.primaryText} />)}
                </DropDownMenu>
              </Hide>
              <CodeText return     = {this.filter}/>
            </Flex>
          : null}
        <Paper style={{position: 'relative', marginBottom: '6rem'}}>
          {this.state.actions.length > 0
            ? <List>
                {map(this.state.actions, (action) =>
                  <ActionLI {...action} key={action.id} update={this.getActions}/>)}
                {this.state.more
                  ? <ListItem style={{
                      marginBottom:  '-1rem',
                      width:         '100%',
                      padding:       '1rem',
                      color:         '#333',
                      textTransform: 'uppercase',
                      borderTop:     '1px solid rgba(0,0,0,.1)'}}
                      onTouchTap = {this.more}>
                      Загрузить ещё
                    </ListItem>
                  : null}
              </List>
              : <div style={{padding: '3rem'}}>Нет мероприятий по заданным параметрам</div>}
          <Hide for={['Куратор МКС', 'Обозреватель']}>
            <ActionNew right = {this.state.actions.length > 6}/>
          </Hide>
        </Paper>
      </Container>
    )
  }
})

export default Actions
