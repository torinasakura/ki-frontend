import React from 'react'
import { Paper, FlatButton } from 'material-ui'

// import Money from '../../Components/Money'
import Interpunct from '../../Components/Interpunct'
import Hide from '../../Components/Hide'
import AddToPlan from './AddToPlan'
import ChangePBS from './ChangePBS'
import { map } from 'lodash'

const ActionHeader = (action) =>
  <Paper style={{padding: '3rem'}}>
    <h1>{
      action['Общие сведения']['Наименование мероприятия'] ||
      action['Общие сведения']['Уникальный номер мероприятия']}
    </h1>
    <div>
    <Interpunct style={{color: 'gray', marginTop: '-1rem'}}>
      {action['Общие сведения']['Наименование государственного органа, осуществляющего реализацию мероприятия']}
      {action.system['Департамент']}
    </Interpunct>
    <Hide for={['Куратор МКС', 'Обозреватель']}>
      <AddToPlan
        actionID = {action.system.uuid}
        current  = {action.system['Год']}/>
      <ChangePBS
        actionID = {action.system.uuid} />
      <FlatButton label='Подписать' disabled/>
    </Hide>
    </div>
  </Paper>

export default ActionHeader
