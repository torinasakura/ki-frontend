import Actions from '../Actions'

export default function getRoutes() {
  return {
    component: Actions,
    path: 'actions'
  }
}