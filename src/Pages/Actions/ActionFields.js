import React from 'react'
import Flex from '../../Components/Flex'
import { map, isNull, isUndefined } from 'lodash'

export const ActionField = ({name, value}) =>
  <Flex className = 'ActionFieldBlock'>
    <div style={{width: '38.2%', flexShrink: 0, marginRight: '2rem'}}>{name}</div>
    <div style={{color: 'gray'}}>{value == null
                                    ? 'Не указано'
                                    : value}</div>
  </Flex>

export const ActionFieldArray = ({name, value}) =>
  <Flex className = 'ActionFieldBlock'>
    <div style={{width: '38.2%', flexShrink: 0, marginRight: '2rem'}}>{name}</div>
    <div style={{color: 'gray'}}>
      {value == null || value.length === 0
        ? 'Не указано'
        : map(value, (item, i) =>
          <div key={i} style={i + 1 < value.length ? {marginBottom: '1rem'} : null}>{item}</div>)}
    </div>
  </Flex>
