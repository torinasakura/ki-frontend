import React from 'react'
import Container from '../../Components/Container'
import Section from '../../Components/Section'
import Form from '../../Form/Form3'
import ActionHeader from './ActionHeader'
import { ActionForm2 } from './ActionForm'
import { map, size, isEqual, omit, pullAt, clone, get, find, remove } from 'lodash'
import { FloatingActionButton, FlatButton } from 'material-ui'
import ContentSave from 'material-ui/svg-icons/content/save'
import axios from 'axios'
import Text from '../../Form/Fields/Text'
import Select from '../../Form/Fields/Select'
import MyDate from '../../Form/Fields/Date'
import File from '../../Form/Fields/File'
import Toggle from '../../Form/Fields/Toggle'
import Money from '../../Components/Money'

const Doc = [{
  name: 'Тип документа',
  type: 'select',
  options: [
    'Указ',
    'Приказ',
    'Постановление',
    'Распоряжение',
    'Внутренний документ',
    'Закон',
    'Федеральный закон',
    'Иное'
  ]
}, {
  name: 'Дата',
  type: 'date',
}, {
  name: 'Номер',
  type: 'text',
}, {
  name: 'Наименование',
  type: 'text',
}, {
  name:    'Орган государственной власти, принявший документ',
  type:    'select',
  options: () => axios.get(`dics/dictionaries/ogv`).then((response) =>
                map(response.data, (ogv) => ogv.name))
}, {
  name: 'Файл',
  type: 'file',
  }
]

const InfoDoc = [{
  name: 'Тип документа',
  type: 'select',
  options: [
    'Указ',
    'Приказ',
    'Постановление',
    'Распоряжение',
    'Внутренний документ'
  ]
}, {
  name: 'Наименование',
  type: 'text',
}, {
  name: 'Статус документа по информатизации',
  type: 'text',
}, {
  name: 'Файл',
  type: 'file',
  }
]

const components = {
  'text':   Text,
  'select': Select,
  'date':   MyDate,
  'toggle': Toggle,
  'file':   File,
  // 'array':  MyArray
}

const Static = ({name, value}) =>
  <div style={{margin:'2rem 0'}}>
    <div style = {{fontSize:'13px', color: 'rgba(0, 0, 0, 0.498039)'}}>{name}</div>
    <div style = {{fontSize: '2rem'}}>{value || 'Нет значения'}</div>
  </div>

const Block = (block) => {
  const Component = components[block.type]
  return (
    <div>
      <Component
        {...block}
        value    = {block.i !== undefined
                      ? block.values[block.section][block.name][block.i]
                      : block.values[block.section][block.name]}
        setValue = {block.setValue.bind(null, block.section, block.name, block.i)} />
    </div>
  )
}

const StrArray = ({title, addLabel, values, section, name, block, setValue}) =>
  <div className='array'>
    <div  className='array__title'>{title || name}</div>
    {map(get(values, [section, name]), (item, i) =>
      <div className = 'array__item' key = {i}>
        <div
          className = 'array__item__separator'
          onClick   = {setValue.bind(null, section, name, i, null)}>
          <div className='array__item__separator__remove'>
            <div className='array__item__separator__remove__cross'>+</div>
          </div>
        </div>
        <Block
          {...block}
          setValue = {setValue}
          values   = {values}
          section  = {section}
          name     = {name}
          i        = {i}
          key      = {i} />
        </div>
      )}
    <FlatButton
      primary
      onTouchTap = {setValue.bind(null, section, name, get(values, [section, name, 'length'], 0), '')}
      label      = {addLabel}
      style      = {{marginLeft:'-2rem'}}/>
  </div>

const ArrBlock = (block) => {
  const Component = components[block.type]
  return (
    <div style={block.style}>
      <Component
        {...block}
        value    = {block.value}
        setValue = {block.setValue.bind(null, block.name)} />
    </div>
  )
}

const ObjArray = React.createClass({
  setValue(i, name, value) {
    let newValue   = this.props.values[this.props.section][this.props.name][i] || {}
    newValue[name] = value
    this.props.setValue(this.props.section, this.props.name, i, newValue)
  },
  render() {
    const {title, addLabel, values, section, name, blocks, setValue} = this.props
    return (
      <div className='array'>
        <div className='array__title'>{title || name}</div>
        {map(get(values, [section, name], []), (item, i) =>
          <div className = 'array__item block' key={i}>
            <div
              className      = 'array__item__separator'
              onClick        = {setValue.bind(null, section, name, i, null)}>
              <div className = 'array__item__separator__remove'>
                <div className = 'array__item__separator__remove__cross'>+</div>
              </div>
            </div>
            {map(blocks, (block, j) =>
              <ArrBlock
                {...block}
                value    = {values[section][name][i][block.name]}
                key      = {j}
                setValue = {this.setValue.bind(null, i)}
              />
              )}
            </div>
          )}
        <FlatButton
          primary
          onTouchTap = {setValue.bind(null, section, name, get(values, [section, name, 'length'], 0), {})}
          label      = {addLabel}
          style      = {{marginLeft:'-2rem'}}/>
      </div>
    )
  }
})

const Rate = React.createClass({
  componentWillMount() {
    axios.get('dics/dictionaries/rates_and_indicators', {
                params: {
                    action_type: 'like.*' + this.props.values['Общие сведения']['Тип мероприятия'].toLowerCase() + '*',
                    category:    'like.*' + this.props.values['Общие сведения']['Объект учета']['Идентификатор'].match(/\d*/) + '*'
                  }
                }).then((response) =>
                  this.setState({rates: response.data}))
  },
  remove() {
    this.props.setValue('Сведения о целевых показателях и индикаторах', 'Показатели и индикаторы', this.props.i, null)
  },
  setValue(name, value) {
    let newRate   = this.props.rate
    newRate[name] = value
    if (name === 'Наименование показателя') {
      const unit = find(this.state.rates, rate => rate.rate_name === value).unit
      newRate['Единица измерения показателя'] = unit
        if (unit === 'Да/нет') {
          newRate = omit(newRate, ['Наименование индикатора','Плановое значение индикатора на первый год планового периода','Плановое значение индикатора на второй год планового периода', ])
      } else {
        newRate['Наименование индикатора'] = find(this.state.rates, rate => rate.rate_name === value).indicator_name
        newRate = omit(newRate, ['Базовое значение', 'Плановое значение показателя на очередной год', 'Плановое значение показателя на первый год планового периода','Плановое значение показателя на второй год планового периода'])
        }
    }
    if (newRate['Единица измерения показателя'] !== 'Да/нет') {
      newRate['Плановое значение индикатора на первый год планового периода'] =
        newRate['Плановое значение показателя на очередной год'] && newRate['Базовое значение']
          ? newRate['Базовое значение'] > 0
            ? ((newRate['Плановое значение показателя на очередной год'] - newRate['Базовое значение']) / newRate['Базовое значение'] * 100) + '%'
            : 'Не рассчитывается'
          : ''
      newRate['Плановое значение индикатора на второй год планового периода'] =
        newRate['Плановое значение показателя на первый год планового периода'] && newRate['Плановое значение показателя на очередной год']
          ? newRate['Базовое значение'] > 0 || newRate['Плановое значение показателя на очередной год'] > 0
            ? ((newRate['Плановое значение показателя на первый год планового периода'] - newRate['Плановое значение показателя на очередной год']) / newRate['Плановое значение показателя на очередной год'] * 100) + '%'
            : 'Не рассчитывается'
          : ''
    }
    this.props.setValue('Сведения о целевых показателях и индикаторах', 'Показатели и индикаторы', this.props.i, newRate)
  },
  render() {
    const isBool = this.props.rate['Единица измерения показателя'] === 'Да/нет'
    return (
      <div className = 'array__item block' >
        <div
          className = 'array__item__separator'
          onClick   = {this.remove}>
          <div className = 'array__item__separator__remove'>
            <div className = 'array__item__separator__remove__cross'>+</div>
          </div>
        </div>
        <ArrBlock
          type     = 'select'
          setValue = {this.setValue}
          name     = 'Наименование показателя'
          options  = {this.state ? map(this.state.rates, rate => rate.rate_name) : []}
          value    = {this.props.rate['Наименование показателя']}/>
        {isBool ? null : <Static
          name  = 'Наименование индикатора'
          value = {this.props.rate['Наименование индикатора']}  />}
        {isBool
          ? <div>
              <ArrBlock
                type     = 'toggle'
                setValue = {this.setValue}
                name     = 'Базовое значение'
                value    = {this.props.rate['Базовое значение']}/>
              <ArrBlock
                type     = 'toggle'
                setValue = {this.setValue}
                name     = 'Плановое значение показателя на очередной год'
                value    = {this.props.rate['Плановое значение показателя на очередной год']}/>
              <ArrBlock
                type     = 'toggle'
                setValue = {this.setValue}
                name     = 'Плановое значение показателя на первый год планового периода'
                value    = {this.props.rate['Плановое значение показателя на первый год планового периода']}/>
              <ArrBlock
                type     = 'toggle'
                setValue = {this.setValue}
                name     = 'Плановое значение показателя на второй год планового периода'
                value    = {this.props.rate['Плановое значение показателя на второй год планового периода']}/>
            </div>
          : <div>
              <div style={{display:'flex'}}>
                <ArrBlock
                  style    = {{flexGrow:'1'}}
                  type     = 'text'
                  setValue = {this.setValue}
                  name     = 'Базовое значение'
                  format   = 'number'
                  value    = {this.props.rate['Базовое значение']}/>
                <ArrBlock
                  disabled
                  type     = 'text'
                  setValue = {this.setValue}
                  name     = 'Единица измерения'
                  value    = {this.props.rate['Единица измерения показателя']}/>
              </div>
            <ArrBlock
              type     = 'text'
              setValue = {this.setValue}
              format   = 'number'
              name     = 'Плановое значение показателя на очередной год'
              value    = {this.props.rate['Плановое значение показателя на очередной год']}/>
            <div style={{display:'flex'}}>
              <ArrBlock
                style    = {{flexGrow:'1'}}
                type     = 'text'
                setValue = {this.setValue}
                name     = 'Плановое значение показателя на первый год планового периода'
                format   = 'number'
                value    = {this.props.rate['Плановое значение показателя на первый год планового периода']}/>
              <ArrBlock
                type     = 'text'
                disabled
                setValue = {this.setValue}
                label    = 'Индикатор'
                name     = 'Плановое значение индикатора на первый год планового периода'
                value    = {this.props.rate['Плановое значение индикатора на первый год планового периода']}/>
            </div>
            <div style={{display:'flex'}}>
              <ArrBlock
                style    = {{flexGrow:'1'}}
                type     = 'text'
                setValue = {this.setValue}
                format   = 'number'
                name     = 'Плановое значение показателя на второй год планового периода'
                value    = {this.props.rate['Плановое значение показателя на второй год планового периода']}/>
              <ArrBlock
                type     = 'text'
                disabled
                setValue = {this.setValue}
                label    = 'Индикатор'
                name     = 'Плановое значение индикатора на второй год планового периода'
                value    = {this.props.rate['Плановое значение индикатора на второй год планового периода']}/>
            </div>
          </div>
        }
      </div>
    )
  }
})


const ActionEdit = React.createClass({

  contextTypes: {
    error: React.PropTypes.func.isRequired,
  },

  getInitialState() {
    return {
      data:     this.props.action,
      showSave: false
    }
  },

  returns(section, name, i, value) {
    const isArray = value === undefined
    let data      = this.state.data
    if(data[section] === undefined)
      data[section] = {}
    if (data[section][name] === undefined)
      isArray
        ? data[section][name] = ''
        : data[section][name] = []
    isArray
      ? data[section][name] = i
      : value === null // Remove from array
        ? pullAt(data[section][name], i)
        : data[section][name][i] = value
    this.setState({showSave: true, data})
    // this.update()
  },

  patchAction() {
    axios.patch('events/' + this.props.params.slug, omit(this.state.data, ['system', 'id'])).then(
      (response) => response.data.error
        ? this.context.error(response.data.error)
        : this.setState({showSave: false})
    )
  },

  render() {
    return (
      <Container>
        <ActionHeader {...this.props.action} />
          <Section
            info = {size(this.state.data['Общие сведения']) + '/' + 11}
            title = 'Общие сведения'>
            <Static
              name  = 'Наименование мероприятия'
              value = {this.state.data['Общие сведения']['Наименование мероприятия']} />
            <Static
              name  = 'Тип мероприятия'
              value = {this.state.data['Общие сведения']['Тип мероприятия']} />
            <Static
              name  = 'Наименование государственного органа, осуществляющего реализацию мероприятия'
              value = {this.state.data['Общие сведения']['Наименование государственного органа, осуществляющего реализацию мероприятия']} />
            <Static
              name  = 'Обьект учета'
              value = {this.state.data['Общие сведения']['Объект учета']['Идентификатор'] + ' — ' + this.state.data['Общие сведения']['Объект учета']['Наименование']} />
            <Static
              name  = 'Уникальный номер мероприятия'
              value = {this.state.data['Общие сведения']['Уникальный номер мероприятия']} />
            <Select
              setValue = {this.returns.bind(null, 'Общие сведения', 'Планируемый статус объекта учета')}
              name     = 'Планируемый статус объекта учета'
              value    = {this.state.data['Общие сведения']['Планируемый статус объекта учета']}
              options  = {['Подготовка к созданию',
                          'Разработка',
                          'Эксплуатация',
                          'Выведен из эксплуатации']}/>
            <Text
              multiline
              setValue  = {this.returns.bind(null, 'Общие сведения', 'Сведения об общедоступной информации объекта учета')}
              name      = 'Сведения об общедоступной информации объекта учета'
              value     = {this.state.data['Общие сведения']['Сведения об общедоступной информации объекта учета']}/>
            <Text
              multiline
              setValue = {this.returns.bind(null, 'Общие сведения', 'Ответственный за реализацию мероприятия')}
              name     = 'Ответственный за реализацию мероприятия'
              value    = {this.state.data['Общие сведения']['Ответственный за реализацию мероприятия']}/>
            <Text
              setValue  = {this.returns.bind(null, 'Общие сведения', 'Дополнительная информация')}
              name      = 'Дополнительная информация'
              multiline = {true}
              value     = {this.state.data['Общие сведения']['Дополнительная информация']}/>
            <StrArray
              values   = {this.state.data}
              setValue = {this.returns}
              addLabel = 'Добавить приоритетное направление'
              section  = 'Общие сведения'
              name     = 'Приоритетное направление'
              block    = {{type: 'select',
                           options: ['1 - Использование информационно-коммуникационных технологий для оптимизации процедур и повышения качества предоставления государственных услуг и исполнения государственных функций, в том числе с применением механизмов получения от граждан и организаций в электронном виде информации о качестве взаимодействия с федеральными органами исполнительной власти и органами управления государственными внебюджетными фондами',
                                     '2 - Использование типовых информационно-технологических сервисов и единой сети передачи данных, а также системы центров обработки данных',
                                     '3 - Использование российских информационно-коммуникационных технологий и свободного программного обеспечения',
                                     '4 - Защита информации, содержащейся в государственных информационных системах, и обеспечение информационной безопасности при использовании информационно-коммуникационных технологий в деятельности федеральных органов исполнительной власти и органов управления государственными внебюджетными фондами',
                                     '5 - Повышение качества и обеспечение доступности государственных информационных ресурсов, в том числе в форме открытых данных',
                                     'Отсутствует']}}/>
            <ObjArray
              values   = {this.state.data}
              setValue = {this.returns}
              section  = 'Общие сведения'
              name     = 'Документ-основания'
              addLabel = 'Добавить документ-основание'
              blocks   = {Doc}           />
          </Section>
          <Section
            info = {size(this.state.data['Государственные услуги (функции) государственного органа']) + '/' + 3}
            title = 'Государственные услуги (функции) государственного органа'>
            <StrArray
              values   = {this.state.data}
              setValue = {this.returns}
              addLabel = 'Добавить госуслугу'
              section  = 'Государственные услуги (функции) государственного органа'
              name     = 'Государственные услуги'
              block    = {{type:'select',
                          options:     () => axios.get('dics/dictionaries/government_services').then((response) =>
                            map(response.data, item =>
                              item.number + ' - ' + item.short_name
                            ))}}/>
            <StrArray
              values   = {this.state.data}
              setValue = {this.returns}
              addLabel = 'Добавить госфункцию'
              section  = 'Государственные услуги (функции) государственного органа'
              name     = 'Государственные функции'
              block    = {{type:'text', multiLine: true}}/>
            <ObjArray
              values   = {this.state.data}
              setValue = {this.returns}
              section  = 'Государственные услуги (функции) государственного органа'
              name     = 'Основание для исполнения государственной функции'
              addLabel = 'Добавить документ-основание'
              blocks   = {Doc}/>
          </Section>
          <Section
            info = {size(this.state.data['Сведения о документах по информатизации']) + '/' + 2}
            title = 'Сведения о документах по информатизации'>
            <ObjArray
              values   = {this.state.data}
              setValue = {this.returns}
              section  = 'Сведения о документах по информатизации'
              name     = 'Документ по информатизации'
              addLabel = 'Добавить документ по информатизации'
              blocks   = {InfoDoc}/>
            <ObjArray
              values   = {this.state.data}
              setValue = {this.returns}
              section  = 'Сведения о документах по информатизации'
              name     = 'Обоснование мероприятия по информатизации'
              addLabel = 'Добавить документ-основание'
              blocks   = {Doc}/>
          </Section>
          <Section
            info = {size(this.state.data['Сведения о целевых показателях и индикаторах']) + '/' + 1}
            title = 'Сведения о целевых показателях и индикаторах'>
            {map(get(this.state.data, ['Сведения о целевых показателях и индикаторах','Показатели и индикаторы'], []), (rate, i) =>
              <Rate
                rate     = {rate}
                i        = {i}
                key      = {i}
                values   = {this.state.data}
                setValue = {this.returns}/>
            )}
          <FlatButton
            primary
            onTouchTap = {this.returns.bind(
              null,
              'Сведения о целевых показателях и индикаторах',
              'Показатели и индикаторы',
              get(this.state.data, ['Сведения о целевых показателях и индикаторах', 'Показатели и индикаторы', 'length'],
              0), {})}
            label = 'Добавить показатель\индикатор'
            style = {{marginLeft:'-2rem'}}/>
          </Section>
          <Section
            info = {size(this.state.data['Сведения о товарах, работах, услугах']) + '/' + 3}
            title = 'Сведения о товарах, работах, услугах'>
            Чтобы получить данные о товарах, работах, услугах переведите мероприятие в статус "Направлено в ЭБ"
            </Section>
          <Section
            info = {size(this.state.data['Сведения об объемах бюджетных ассигнований']) + '/' + 4}
            title = 'Сведения об объемах бюджетных ассигнований'>
            Чтобы получить данные о бюджетных ассигнованиях переведите мероприятие в статус "Направлено в ЭБ"
            </Section>
        <Section
          info = {size(this.state.data['Сведения о необходимости проведения оценки мероприятия по информатизации']) + '/' + 4}
          title = 'Сведения о необходимости проведения оценки мероприятия по информатизации'>
          <Select
            setValue = {this.returns.bind(null, 'Сведения о необходимости проведения оценки мероприятия по информатизации', 'Вид закупки')}
            name     = 'Вид закупки'
            value    = {get(this.state.data, ['Сведения о необходимости проведения оценки мероприятия по информатизации','Вид закупки'])}
            options  = {[
              'Обеспечение эксплуатации',
              'Общетехнологические нужды'
            ]}/>
          <Static
            name  = 'Сведения об оценке мероприятия по информатизации'
            value = {get(this.state.data, ['Сведения о необходимости проведения оценки мероприятия по информатизации', 'Сведения об оценке мероприятия по информатизации'])} />
          <Static
            name  = 'Изменение параметров мероприятия по информатизации'
            value = {get(this.state.data, ['Сведения о необходимости проведения оценки мероприятия по информатизации','Изменение параметров мероприятия по информатизации'])} />
          <Static
            name  = 'Оценка мероприятия по информатизации'
            value = {get(this.state.data, ['Сведения о необходимости проведения оценки мероприятия по информатизации','Оценка мероприятия по информатизации'])} />
          </Section>
          {this.state.showSave
            ? <FloatingActionButton onClick={this.patchAction} style={{position: 'fixed', right: '3rem', bottom: '3rem'}} >
                <ContentSave/>
              </FloatingActionButton>
            : null}
      </Container>
    )
  }
})

export default ActionEdit
