import React from 'react'
import { FloatingActionButton, Dialog, RaisedButton, FlatButton } from 'material-ui'
import ContentAdd from 'material-ui/svg-icons/content/add'
import Form from '../../Form/Form3'
import axios from 'axios'
import { map, remove, uniqueId, get } from 'lodash'
import Select from '../../Form/Fields/Select'
import Text from '../../Form/Fields/Text'

const pad = (num, size) => {
    const s = "000" + num;
    return s.substr(s.length-size)
}

const type = {
	"Создание": [
		"Создание"
	],
	"Подготовка к созданию": [
		"Создание",
		"Вывод из эксплуатации"
	],
	"Разработка": [
		"Создание",
		"Вывод из эксплуатации"
	],
	"Эксплуатация": [
		"Развитие",
		"Эксплуатация",
		"Вывод из эксплуатации"
	],
	"Ввод в эксплуатацию": [
		"Создание",
		"Вывод из эксплуатации"
	],
	"Вывод из эксплуатации": [
		"Вывод из эксплуатации"
	],
	"Развитие": [
		"Развитие"
	]
}

const NewActionForm = [
  {
    name:        'Объект учета',
    type:        'select',
    optionLabel: (option) => option['Идентификатор'] + ' - ' + option['Наименование'],
    options:     () =>
      axios.post('http://reg.eskigov.ru/api/v1/entity', {
        entity_type: 'node',
        api_key:     'test_key',
        base_info:   true,
        ogv_code:    pad(JSON.parse(localStorage.getItem('user')).user.grbs, 3) === '000' ? '071' : pad(JSON.parse(localStorage.getItem('user')).user.grbs, 3), //FIXME Remove after migration
        condition:   {
          property: {
            status: '1'
          }
        }
      }).then((response) => map(response.data.data, (obj, key) => {
        let result = {}
        result = {
          'Идентификатор':               obj.registry_num,
          'Наименование':                obj.title,
          'Классификационная категория': obj.category,
          'Статус':                      obj.status
        }
        return result
      }))
  }, {
    name:    'Тип мероприятия',
    type:    'select',
    show:    (values) => values['Объект учета'] ? true : false,
    options: (values, moreData) => type[values['Объект учета']['Статус']]
  }, {
    name: 'Получатель бюджетных средств',
    type: 'text',
    show: (values) => values['Объект учета'] ? true : false,
  }
]

const ActionNew = React.createClass({

  contextTypes: {
    router: React.PropTypes.object.isRequired,
    error:  React.PropTypes.func.isRequired,
  },

  getInitialState() {
    return ({
      action: {
        'Общие сведения': {}
      },
      open: false
    })
  },

  create() {
    axios.post('events', this.state.action).then((response) =>
      response.data.error
        ? this.context.error(response.data.error)
        : this.context.router.push('/actions/' + response.data.uuid + '/edit'))
  },

  returns(key, value) {
    let action = this.state.action
    key === 'Получатель бюджетных средств'
      ? value.length <= 8
        ? action[key] = value
        : null
      : action['Общие сведения'][key] = value
    if (key === 'Объект учета')
      action['Общие сведения']['Тип мероприятия'] = ''
    this.setState({action})
  },

  toggle() {
    this.setState({open: !this.state.open})
  },

  render() {
    return (
      <div>
      <FloatingActionButton
          onTouchTap = {this.toggle}
          style      = {{
            right:    this.props.right ? '4rem' : 'calc(50% - 3.5rem)',
            bottom:   this.props.right ? '3rem' : '-3.5rem',
            position: this.props.right ? 'fixed' : 'absolute'}} >
        <ContentAdd />
      </FloatingActionButton>
      <Dialog
        autoDetectWindowHeight
        title     = 'Новое мероприятие'
        bodyStyle = {{overflowY: 'visible'}}
        open      = {this.state.open}>
        <Select
          setValue    = {this.returns.bind(null, 'Объект учета')}
          name        = 'Объект учета'
          value       = {this.state.action['Общие сведения']['Объект учета']}
          optionLabel = {(option) => option['Идентификатор'] + ' - ' + option['Наименование']}
          options     = {() =>
            axios.post('http://reg.eskigov.ru/api/v1/entity', {
              entity_type: 'node',
              api_key:     'test_key',
              base_info:   true,
              ogv_code:    pad(JSON.parse(localStorage.getItem('user')).user.grbs, 3) === '000' ? '071' : pad(JSON.parse(localStorage.getItem('user')).user.grbs, 3), //FIXME Remove after migration
              condition:   {
                property: {
                  status: '1'
                }
              }
            }).then((response) => map(response.data.data, (obj, key) => {
              let result = {}
              result = {
                'Идентификатор':               obj.registry_num,
                'Наименование':                obj.title,
                'Классификационная категория': obj.category,
                'Статус':                      obj.status
              }
              return result
            }))} />
        {this.state.action['Общие сведения']['Объект учета']
          ? <div>
              <Select
                setValue = {this.returns.bind(null, 'Тип мероприятия')}
                name     = 'Тип мероприятия'
                value    = {this.state.action['Общие сведения']['Тип мероприятия']}
                options  = {() => type[get(this.state.action, ['Общие сведения', 'Объект учета', 'Статус'], null)] || ''}/>
              <Text
                setValue  = {this.returns.bind(null, 'Получатель бюджетных средств')}
                name      = 'Получатель бюджетных средств'
                value     = {this.state.action['Получатель бюджетных средств'] || ''}/>
              <span style={{lineHeight:'1.618em', fontSize: 11}}>Уникальный код получателя бюджетных средств по реестру участников бюджетного процесса. Справочная информация по кодам находится на портале Электронного бюджета в закладке "Реестр участников бюджетного процесса, а также юридических лиц, не являющихся участниками бюджетного процесса"</span>
            </div>
          : null }
          <br/>
        <RaisedButton primary label='Создать' onTouchTap={this.create}/>
        <FlatButton primary label='Отменить' onTouchTap={this.toggle}/>
      </Dialog>
    </div>
      )
  }
})

export default ActionNew
