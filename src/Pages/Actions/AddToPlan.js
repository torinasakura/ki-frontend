import React, { PropTypes } from 'react'
import { DropDownMenu, MenuItem, IconMenu, SelectField, IconButton, Menu, FlatButton, Popover} from 'material-ui'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import { map, filter } from 'lodash'
import axios from 'axios'

const AddToPlan = React.createClass({

  contextTypes: {
    error: React.PropTypes.func.isRequired,
    user:  React.PropTypes.func.isRequired
  },

  componentWillMount() {
    axios.get('plans', {
      params: {
        grbs:       this.context.user().user.grbs,
        department: this.context.user().user.role !== 'Куратор ОГВ' ? this.context.user().user.dept : null
      }})
    .then((response) => {
      this.setState({plans: filter(response.data, item => item['Департамент'] !== undefined)})
      }
    )
  },

  setPlan(year) {
    axios({
      method:  'put',
      url:     `events/${this.props.actionID}/year`,
      headers: {'Content-Type': 'application/json'},
      data:    {year}
    }).then((response) =>
      response.data.error
        ? this.context.error(response.data.error)
        : this.setState({current: year}))
    this.toggle(event)
  },

  getInitialState() {
    return {
      current: this.props.current,
      open:    false,
      plans:   [2017, 2018, 2019]
    }
  },

  toggle(event) {
    this.setState({
      open:     !this.state.open,
      anchorEl: event.currentTarget,
    })
  },

  render () {
    return (
      <span style = {this.props.style}>
        <FlatButton
          onTouchTap = {this.toggle}
          label      = {this.state.current ? `В плане ${this.state.current} - ${this.state.current + 2}` : 'Добавить в план'}
          style      = {{marginLeft:'-2rem', marginTop:'-.5rem'}}/>
        <Popover
          open           = {this.state.open}
          anchorEl       = {this.state.anchorEl}
          anchorOrigin   = {{horizontal: 'left', vertical: 'top'}}
          targetOrigin   = {{horizontal: 'left', vertical: 'top'}}
          onRequestClose = {this.toggle}>
          {this.state.plans
            ? map(this.state.plans, (plan, i) =>
              <MenuItem
                key         = {i}
                onTouchTap  = {this.setPlan.bind(null, plan['Год начала планового периода'])}
                value       = {plan['Год начала планового периода']}
                primaryText = {plan['Департамент'] + ' на  ' + plan['Год начала планового периода'] + ' - ' + (plan['Год начала планового периода'] + 2)} />)
            : null}
        </Popover>
      </span>
    )
  }
})

export default AddToPlan
