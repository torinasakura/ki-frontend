import React from 'react'
import { Dialog, FlatButton, TextField, RaisedButton } from 'material-ui'
import axios from 'axios'

const ChangePBS = React.createClass({

    contextTypes: {
      error:  React.PropTypes.func.isRequired
    },

  getInitialState() {
    return {
      open: false,
      pbs:  ''
    }
  },

  toggle() {
    this.setState({open: !this.state.open})
  },

  update() {
    axios.put(`events/${this.props.actionID}/pbs`, {pbs: this.state.pbs}).then(response =>
      response.data.error
        ? this.context.error(response.data.error)
        : this.setState({open: !this.state.open})
      )
  },

  handleChange(e) {
    e.target.value.length <= 8
      ? this.setState({pbs: e.target.value})
      : null
  },

  render() {
    return (
      <span>
        <Dialog
          title          = 'Смена ПБС в коде мероприятия'
          onRequestClose = {this.toggle}
          open           = {this.state.open} >
          <TextField
            floatingLabelText = 'Новый ПБС'
            onChange          = {this.handleChange}
            value             = {this.state.pbs}
            fullWidth />
          <br/>
          <br/>
          <RaisedButton primary onTouchTap={this.update} label='Сменить ПБС'/>
          <FlatButton onTouchTap={this.toggle} label='Отменить'/>
        </Dialog>
        <FlatButton onTouchTap={this.toggle} label='Сменить ПБС' />
      </span>
    )
  }
})

export default ChangePBS
