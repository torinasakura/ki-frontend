import React, { Component } from 'react'
import { DropDownMenu, MenuItem, Divider, IconButton} from 'material-ui'
import { map,
         forEach,
         isBoolean,
         isObject,
         isArray,
         get,
         size,
         omit,
         merge,
         concat,
         last,
         isEqual,
         uniqueId } from 'lodash'
import FileFolder from 'material-ui/svg-icons/file/folder'
import axios from 'axios'
import Container from '../../Components/Container'
import Menu from '../../Components/Menu'
import { Link } from 'react-router'

const ActionMenu = React.createClass({

  contextTypes: {
    error: React.PropTypes.func.isRequired,
  },

  componentWillMount() {
    axios({
      method: 'get',
      url:    `status/${this.props.uuid}`,
      params: {
        role: this.props.role
      }
    }).then((response) =>
      this.setState({
        status:      response.data.status,
        transitions: concat([], response.data.transitions, response.data.status)
      })
    )
  },

  changeStatus(event, i, nextStatus) {
    if (nextStatus !== 'history')
      axios.patch(`/events/${this.props.uuid}/change_status`, {
        status: nextStatus
      }).then((response) => response.data.error
        ? this.context.error(response.data.error)
        : this.setState({
            status:      response.data.status,
            transitions: concat([], response.data.transitions, response.data.status)
          }))
  },

  render() {
    return (
      <div style   = {{flexGrow: '1', display: 'flex', alignItems: 'center'}}>
        <Link to = {`/actions/${this.props.uuid}`}>Мероприятие</Link>
        <DropDownMenu
              labelStyle     = {{color: 'white'}}
              underlineStyle = {{display: 'none'}}
              value          = {this.state ? this.state.status : 'Статус'}
              onChange       = {this.changeStatus}
              style = {{height: 'auto'}}>
              {this.state ? map(this.state.transitions, (transition) =>
                <MenuItem
                  key         = {transition}
                  value       = {transition}
                  primaryText = {transition}/>) : <MenuItem primarText='Статус' value='Статус'/>}
            <Divider />
            <MenuItem value='history' primaryText ='История статусов' disabled/>
          </DropDownMenu>
          <Link to = {`/actions/${this.props.uuid}/files`}>
            <IconButton tooltip='Файлы'>
              <FileFolder color = 'white' />
            </IconButton>
          </Link>
      </div>
    )
  }
})

const Action = React.createClass({

  contextTypes: {
    error:      React.PropTypes.func.isRequired,
    user:       React.PropTypes.func.isRequired,
    router:     React.PropTypes.object
  },

  fetchAction() {
    axios({
      method:  'get',
      url:     'events/' + this.props.params.slug
    }).then((response) =>
      response.data.error
        ? this.context.error(response.data.error)
        : this.setState({data: response.data, current: {...response.data}}))
  },

  componentWillMount() {
    this.fetchAction()
  },

  getInitialState() {
    return {
      data:    null,
      current: null,
    }
  },

  render() {
    return (
      <div>
        <Menu back>
          <ActionMenu
            uuid = {this.props.params.slug}
            role = {this.context.user().user.role}/>
        </Menu>
        <Container>
          {this.state.data
            ? this.props.children && React.cloneElement(this.props.children, {
                action: this.state.data,
                update: this.fetchAction})
            : null}
        </Container>
      </div>
    )
  }
})

export default Action
