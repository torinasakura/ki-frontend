import axios from 'axios'
import {
  map,
  forEach,
  get,
  uniqBy,
  concat,
  groupBy,
  merge
} from 'lodash'

let rates = []
const getRates = (values, moreData) =>
  axios.get(
    'dics/dictionaries/rates_and_indicators', {
    params: {
        action_type: 'like.*' + moreData['Общие сведения']['Тип мероприятия'].toLowerCase() + '*',
        category:    'like.*' + moreData['Общие сведения']['Объект учета']['Идентификатор'].match(/\d*/) + '*'
      }
    }).then((response) => rates = response.data)

const Doc = [{
  name: 'Тип документа',
  type: 'select',
  options: [
    'Указ',
    'Приказ',
    'Постановление',
    'Распоряжение',
    'Внутренний документ'
  ]
}, {
  name: 'Дата',
  type: 'date',
}, {
  name: 'Номер',
  type: 'text',
}, {
  name: 'Наименование',
  type: 'text',
}, {
  name: 'Орган государственной власти, принявший документ',
  type: 'select',
  options: (value, moreData) => axios.get(`dics/dictionaries/ogv`).then((response) =>
                map(response.data, (ogv) => ogv.name))
}, {
  name: 'Файл',
  type: 'file',
}]

export const ActionForm2 = {
  'Общие сведения': [{
    name:     'Наименование мероприятия',
    type:     'text',
    disabled: true,
  }, {
    name: 'Тип мероприятия',
    type: 'text',
    disabled: true,
  }, {
    name: 'Уникальный номер мероприятия',
    type: 'text',
    disabled: true,
  }, {
    name: 'Объект учета',
    type: 'select',
    optionLabel: (option) => option['Идентификатор'] + ' - ' + option['Наименование'],
    disabled: true
  }, {
    name: 'Наименование государственного органа, осуществляющего реализацию мероприятия',
    type: 'text',
    disabled: true
  }, {
    name: 'Планируемый статус объекта учета',
    type: 'select',
    options: [
      'Подготовка к созданию',
      'Разработка',
      'Эксплуатация',
      'Выведен из эксплуатации',
    ]
  }, {
    name: 'Сведения об общедоступной информации объекта учета',
    type: 'text',
  }, {
    name: 'Ответственный за реализацию мероприятия',
    type: 'text',
    // optionLabel: (option) => option['ФИО'] + ' - ' + option['Должность'],
    // options: [{
    //   'ФИО': 'Иванов Роман',
    //   'Должность': 'Начальник отдела',
    //   'Телефон': '89232346709',
    //   'E-mail': 'ivanovr@minsvyaz.ru'
    // }, {
    //   'ФИО': 'Иванов Роман',
    //   'Должность': 'Начальник департамента',
    //   'Телефон': '89232346709',
    //   'E-mail': 'ivanovr@minsvyaz.ru'
    // }]
  }, {
    name: 'Дополнительная информация',
    type: 'text',
  }, {
    name: 'Приоритетное направление',
    type: 'array',
    addable: true,
    object: [{
      name: 'Направление',
      type: 'select',
      options: [
        '1 - Использование информационно-коммуникационных технологий для оптимизации процедур и повышения качества предоставления государственных услуг и исполнения государственных функций, в том числе с применением механизмов получения от граждан и организаций в электронном виде информации о качестве взаимодействия с федеральными органами исполнительной власти и органами управления государственными внебюджетными фондами',
        '2 - Использование типовых информационно-технологических сервисов и единой сети передачи данных, а также системы центров обработки данных',
        '3 - Использование российских информационно-коммуникационных технологий и свободного программного обеспечения',
        '4 - Защита информации, содержащейся в государственных информационных системах, и обеспечение информационной безопасности при использовании информационно-коммуникационных технологий в деятельности федеральных органов исполнительной власти и органов управления государственными внебюджетными фондами',
        '5 - Повышение качества и обеспечение доступности государственных информационных ресурсов, в том числе в форме открытых данных',
        'Отсутствует'
      ]
    }]
  }, {
    name: 'Документ-основания',
    type: 'array',
    addable: true,
    object: Doc
  }],

  'Государственные услуги (функции) государственного органа': [{
    name: 'Государственные услуги',
    type: 'array',
    addable: true,
    object: [{
      name:        'Государственная услуга',
      type:        'select',
      options:     (values, moreData) => axios.get('dics/dictionaries/government_services').then((response) =>
        map(response.data, item => item.number + ' - ' + item.short_name))
    }]
  }, {
    name: 'Государственные функции',
    type: 'array',
    addable: true,
    object: [{
      name: 'Государственная функция',
      type: 'text',
      // optionLabel: (option) => option['Код государственной функции'] + ' - ' + option['Наименование государственной функции'],
      // options: [{
      //   'Код государственной функции':          10000028428,
      //   'Наименование государственной функции': 'Актуализация сведений о федеральных государственных информационных системах в реестре',
      // }]
    }]
  }, {
    name: 'Основание для исполнения государственной функции',
    type: 'array',
    addable: true,
    object: Doc
  }],
  'Сведения о документах по информатизации': [{
    name: 'Документ по информатизации',
    type: 'array',
    addable: true,
    object: [{
      name: 'Тип',
      type: 'select',
      options: [
        'Указ',
        'Приказ',
        'Постановление',
        'Распоряжение',
        'Внутренний документ'
      ]
    }, {
      name:  'Наименование',
      type:  'text',
      value: (values, moreData) => values['Тип'] || 'тест'
    },{
      name:  'Mleh',
      type:  'text',
      value: (values, moreData) => values['Наименование'] || 'тест'
    }, {
      name: 'Статус документа по информатизации',
      type: 'select',
      options: [
        'проект',
        'положительное заключение Минкомсвязи России',
        'отрицательное заключение Минкомсвязи России'
      ]
    }, {
      name: 'Файл',
      type: 'file',
    }]
  }, {
    name: 'Обоснование мероприятия по информатизации',
    type: 'array',
    addable: true,
    object: Doc
  }],
  'Сведения о целевых показателях и индикаторах': [{
    name: 'Показатели и индикаторы',
    type: 'array',
    addable: true,
    object: [{
      name: 'Наименование показателя',
      type: 'select',
      options: (values, moreData) => axios.get(
        'dics/dictionaries/rates_and_indicators', {
        params: {
            action_type: 'like.*' + moreData['Общие сведения']['Тип мероприятия'].toLowerCase() + '*',
            category:    'like.*' + moreData['Общие сведения']['Объект учета']['Идентификатор'].match(/\d*/) + '*'
          }
        }).then((response) =>
        map(response.data, item => item.rate_name)
      )
    }, {
      name:     'Единица измерения показателя',
      type:     'text',
      disabled: true,
      value:    (values) =>
        values['Наименование показателя']
          ? axios.get('dics/dictionaries/rates_and_indicators?rate_name=' + values['Наименование показателя'])
              .then((response) => response.data[0].unit)
          : ''
    }, {
      name:   'Базовое значение',
      type:   'text',
      format: 'number'
    }, {
      name:   'Плановое значение показателя на очередной год',
      type:   'text',
      format: 'number'
    }, {
      name:   'Плановое значение показателя на первый год планового периода',
      type:   'text',
      format: 'number'

    }, {
      name:   'Плановое значение показателя на второй год планового периода',
      type:   'text',
      format: 'number'

    }, {
      name:     'Наименование индикатора',
      type:     'text',
      disabled: true,
      show:     (values) => values['Единица измерения показателя'] === 'Да/нет' ? false : true,
      // value:    (values) =>
      //   values['Наименование показателя']
      //     ? axios.get('dics/dictionaries/rates_and_indicators?rate_name=' + values['Наименование показателя'])
      //         .then((response) => response.data[0].indicator_name)
      //     : ''
    }, {
      name:     'Плановое значение индикатора на первый год планового периода',
      type:     'text',
      disabled: true,
      show:     (values) => values['Единица измерения показателя'] === 'Да/нет' ? false : true,
      // value:    (values) =>
      //   values['Плановое значение показателя на очередной год'] && values['Базовое значение']
      //     ? ((values['Плановое значение показателя на очередной год'] - values['Базовое значение']) / values['Базовое значение'] * 100) + '%'
      //     : ''
    }, {
      name:     'Плановое значение индикатора на второй год планового периода',
      type:     'text',
      disabled: true,
      show:     (values) => values['Единица измерения показателя'] === 'Да/нет' ? false : true,
      // value:    (values) =>
      //   values['Плановое значение показателя на первый год планового периода'] && values['Плановое значение показателя на очередной год']
      //     ? ((values['Плановое значение показателя на первый год планового периода'] - values['Плановое значение показателя на очередной год']) / values['Плановое значение показателя на очередной год'] * 100) + '%'
      //     : ''
    }]
  }],
  'Сведения о товарах, работах, услугах': [{
    name: 'Товары, работы, услуги',
    type: 'array',
    object: [{
      name: 'Код товара, работы, услуги',
      type: 'text'
    }, {
      name: 'Наименование заказчика',
      type: 'text'
    }, {
      name: 'Наименование объекта закупки',
      type: 'text'
    }, {
      name: 'Цели закупки',
      type: 'text'
    }, {
      name:        'ОКПД',
      type:        'select',
      optionLabel: (option) => option['Код'] + ' - ' + option['Наименование'],
      options:     () => axios.get(`dics/dictionaries/okpd`).then((response) =>
        map(response.data, (obj, key) => {
          let result = {}
          result = {
            'Код':          obj.okpd_number,
            'Наименование': obj.name,
          }
          return result
        }))
    }, {
      name: 'Объем бюджетных ассигнований на закупку',
      type: 'text'
    }, {
      name: 'Идентификационный код объекта закупки',
      type: 'text'
    }]
  }, {
    name: 'Обоснование закупки',
    type: 'array',
    object: Doc
  }, {
    name: 'Обоснование стоимости закупки',
    type: 'array',
    object: Doc,
  }],
  'Сведения об объемах бюджетных ассигнований': [{
    name: 'Объём бюджетных ассигнований на текущий год',
    type: 'text',
    disabled: true
  }, {
    name: 'Объём бюджетных ассигнований на первый год планового периода',
    type: 'text',
    disabled: true
  }, {
    name: 'Объём бюджетных ассигнований на второй год планового периода',
    type: 'text',
    disabled: true
  }, {
    name: 'Совокупный объём бюджетных ассигнований',
    type: 'text',
    disabled: true
  }],
  'Сведения о необходимости проведения оценки мероприятия по информатизации': [
    {
      name: 'Сведения об оценке мероприятия по информатизации',
      type: 'text',
      disabled: true
    }, {
      name: 'Изменение параметров мероприятия по информатизации',
      type: 'text',
      disabled: true
    }, {
      name: 'Вид закупки',
      type: 'select',
      options: [
        'Обеспечение эксплуатации',
        'Общетехнологические нужды'
      ]
    }, {
      name: 'Оценка мероприятия по информатизации',
      type: 'text',
      disabled: true
    }
  ]
}
