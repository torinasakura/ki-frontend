import React, {PropTypes} from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    cursor: 'pointer',
    width: '100%',
    display: 'block',
  },
  hide: {
    display: 'none'
  }
})

const MenuItem = ({ children, clickHandler, hideIf }) => (
  <div className={styles( {hide: hideIf} )} onClick={clickHandler}>
    {children}
  </div>
)

MenuItem.defaultProps = {
  hideIf: false
}

MenuItem.propTypes = {
  clickHandler: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
}

export default MenuItem