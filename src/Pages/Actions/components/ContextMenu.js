import React, {PropTypes} from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    position: 'relative',
    width: '20px',
  },
})

class ContextMenu extends React.Component {
  constructor() {
    super()
    this.state = {
      visible: false
    }

    this.showMenu = this.showMenu.bind(this)
  }
  showMenu() {
    this.setState({visible: true})
  }
  render() {
    return (
      <div className={styles({ position: this.props.position })} onClick={this.showMenu}>
        {this.props.trigger}
        { visible
            ? <Layer onOutsideClick={this.hideMenu}>
                {this.props.children}
              </Layer>
            : null }
      </div>
    )
  }
}

ContextMenu.propTypes = {
  position: PropTypes.oneOf([
    'top-right',
    'top-left',
    'bottom-left',
    'bottom-right',
  ]),
  children: PropTypes.node,
  trigger: PropTypes.element,
}

export default ContextMenu