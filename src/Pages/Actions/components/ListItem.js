import React, { PropTypes } from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    border: '10px',
    boxSizing: 'border-box',
    display: 'block',
    fontFamily: 'Roboto, sans-serif',
    cursor: 'pointer',
    textDecoration: 'none',
    margin: 0,
    padding: '20px 56px 16px 24px',
    outline: 'none',
    fontSize: '16px',
    fontWeight: 'inherit',
    transform: 'translate(0px, 0px)',
    color: 'rgba(0, 0, 0, 0.870588)',
    lineHeight: '3rem',
    position: 'relative',
    transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
    background: 'none',
    '&:hover': {
      backgroundColor: 'rgba(0,0,0, 0.098392)'
    }
  }
})

const mutedTextStyle = StyleSheet.create({
  self: {
    fontSize: '14px',
    lineHeight: '16px',
    height: '16px',
    margin: '4px 0px 0px',
    color: 'rgba(0, 0, 0, 0.541176)',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  }
})


const ListItem = ({ primaryText, mutedText }) => (
  <div className={styles()}>
    <div>{primaryText}</div>
    <div className={mutedTextStyle()}>{mutedText}</div>
  </div>
)

ListItem.propTypes = {
  primaryText: PropTypes.string,
  mutedText: PropTypes.node,
}

export default ListItem