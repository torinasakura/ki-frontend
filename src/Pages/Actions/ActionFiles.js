import React, { Component } from 'react'
import { Paper, List, ListItem, TextField, RaisedButton, Dialog, FloatingActionButton, IconButton } from 'material-ui'
import EditorInsertDriveFile from 'material-ui/svg-icons/editor/insert-drive-file'
import ActionDelete from 'material-ui/svg-icons/action/delete'
import ContentAdd from 'material-ui/svg-icons/content/add'
import Container from '../../Components/Container'
import axios from 'axios'
import Form from '../../Form/Form3'
import { map, forEach, merge, concat, remove, isEmpty } from 'lodash'

const FileForm = [{
    name: 'uuid',
    type: 'file',
    label: 'Загрузить файл'
  }]

const FileLI = React.createClass({
  componentWillMount() {
    axios({
        method: 'get',
        url:    `/files/${this.props.uuid}/info`}).then(
                (response) => this.setState({file: response.data})
      )
  },

  download() {
    window.open(axios.defaults.baseURL + '/files/' + this.props.uuid)
  },

  render() {

    return this.state
      ? <ListItem
          leftIcon        = {<EditorInsertDriveFile /> }
          primaryText     = {this.state.file.file_name}
          innerDivStyle   = {{overflow:'hidden'}}
          rightIconButton = { <IconButton onTouchTap = {this.props.delete} >
                                <ActionDelete color = 'gray'/>
                              </IconButton>}
          onTouchTap      = {this.download}
          />
      : null
  }
})

const ActionFiles = React.createClass({

  getInitialState() {
    return {
      files:  [],
      dialog: false,
    }
  },

  delete(i) {
    let modifiedFiles = this.props.action.system['Файлы']
    modifiedFiles.splice(i, 1)
    axios.put(`/events/${this.props.action.system.uuid}/files`, modifiedFiles).then((response) =>
      this.props.update()
    )
  },

  new(newFile) {
    this.setState({newFile: newFile.uuid})
  },

  update() {
    axios.put(`/events/${this.props.action.system.uuid}/files`, concat(this.props.action.system['Файлы'] || [], this.state.newFile))
    .then((response) => this.props.update())
    this.toggle()
  },

  toggle() {
    this.setState({dialog: !this.state.dialog})

    // this.getInfo()
  },

  render() {
    const noFiles = isEmpty(this.props.action.system['Файлы'])
    return (
      <Container width = '64rem'>
        <Paper  style={{paddingBottom: '3rem', position:'relative'}}>
          <Dialog
            title          = 'Загрузка нового файла'
            onRequestClose = {this.toggle}
            open           = {this.state.dialog}>
            <Form
              form    = {FileForm}
              returns = {this.new} />

          <RaisedButton label='Загрузить' onClick={this.update}/>

          </Dialog>
          {noFiles
            ? <ListItem onTouchTap={this.toggle}>Нажмите чтобы прикрепить документ</ListItem>
            : <div>
                <List>
                  {map(this.props.action.system['Файлы'], (file, i) =>
                    <FileLI
                      delete = {this.delete.bind(this, i)}
                      uuid   = {file}
                      key    = {i} />)}
                </List>
                <FloatingActionButton onTouchTap={this.toggle} style={{position:'absolute', left: 'calc(50% - 3.5rem)'}} >
                  <ContentAdd />
                </FloatingActionButton>
              </div>}
        </Paper>
      </Container>
    )
  }
})

export default ActionFiles
