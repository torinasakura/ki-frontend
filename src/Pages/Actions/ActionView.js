import React, { Component } from 'react'
import { Paper, List, ListItem, TextField, FloatingActionButton, FlatButton } from 'material-ui'
import ImageEdit from 'material-ui/svg-icons/image/edit'
import Container from '../../Components/Container'
import Interpunct from '../../Components/Interpunct'
import FileLink from '../../Components/FileLink'
import Date from '../../Components/Date'
import Hide from '../../Components/Hide'
import Section from '../../Components/Section'
import ActionHeader from './ActionHeader'
import ActionDocs from './ActionDocs'
import { ActionField, ActionFieldArray } from './ActionFields'
import Form from '../../Form/Form3'
import { ActionForm2 } from './ActionForm'
import axios from 'axios'
import { Link } from 'react-router'
import { map, last, size, get, isArray, isObject, isBoolean, isEqual } from 'lodash'

const valueType = (key, value) => {
  switch (key) {
    case 'Файл':
      return <FileLink uuid={value} />
      break
    case 'Дата':
      return <Date value={value} />
      break
    default:
      return value
  }
}

const checkType = (value, key) => {
  let field = <div>unparseable</div>
  if (isArray(value)) {
    if (isObject(value[0])) {
      field =
      <div className='fieldView' key={key}>
        <div>{key}</div>
        {map(value, (object, objectKey) =>
          <div key       = {objectKey}
               className = 'arrayObject'>
            {map(object, (subValue, subKey) => checkType(subValue, subKey))}</div>)}
      </div>
    } else {
      field =
      <div className='fieldView' key={key}>
        <div>{key}</div>
        {map(value, (subValue, subKey) =>
          <div key={subKey}>{valueType(subKey, subValue)}</div>)}
      </div>
    }
  } else if (isObject(value)) {
    field =
    <div className='fieldView' key={key}>
      <div>{key}</div>
      {map(value, (subValue, subKey) => checkType(subValue, subKey))}
    </div>
  } else {
    field =
    <div className='flexed fieldView' key={key}>
      <div>{key}</div>
      <div>{isBoolean(value) ? value ? 'да' : 'нет' : valueType(key, value)}</div>
    </div>
  }

  return field
}

const ActionView = React.createClass({

  contextTypes: {
    router:     React.PropTypes.object
  },

  edit() {
    this.context.router.push(`/actions/${this.props.params.slug}/edit`)
  },

  render() {
    const action = this.props.action
    return (
      <Container>
        <ActionHeader {...this.props.action} />
          <Section
            padding = {null}
            title   = 'Общие сведения'
            info    = {size(action['Общие сведения']) + '/' + 11}>
            <ActionField
              name  = 'Наименование мероприятия'
              value = {action['Общие сведения']['Наименование мероприятия']}/>
            <ActionField
              name  = 'Тип мероприятия'
              value = {action['Общие сведения']['Тип мероприятия']}/>
            <ActionField
              name  = 'Наименование государственного органа, осуществляющего реализацию мероприятия'
              value = {action['Общие сведения']['Наименование государственного органа, осуществляющего реализацию мероприятия']}/>
            <ActionField
              name  = 'Объект учета'
              value = {action['Общие сведения']['Объект учета']['Идентификатор'] + ' — ' + action['Общие сведения']['Объект учета']['Наименование']}/>
            <ActionField
              name  = 'Уникальный номер мероприятия'
              value = {action['Общие сведения']['Уникальный номер мероприятия']}/>
            <ActionField
              name  = 'Планируемый статус объекта учета'
              value = {action['Общие сведения']['Планируемый статус объекта учета']}/>
            <ActionField
              name  = 'Сведения об общедоступной информации объекта учета'
              value = {action['Общие сведения']['Сведения об общедоступной информации объекта учета']}/>
            <ActionField
              name  = 'Ответственный за реализацию мероприятия'
              value = {action['Общие сведения']['Ответственный за реализацию мероприятия']}/>
            <ActionField
              name  = 'Дополнительная информация'
              value = {action['Общие сведения']['Дополнительная информация']}/>
            <ActionFieldArray
              name  = 'Приоритетные направления'
              value = {action['Общие сведения']['Приоритетное направление']}/>
            <ActionDocs
              name  = 'Документ-основания'
              value = {action['Общие сведения']['Документ-основания']}/>
          </Section>
          <Section
            padding = {null}
            info    = {size(action['Государственные услуги (функции) государственного органа']) + '/' + 3}
            title   = 'Государственные услуги (функции) государственного органа'>
            <ActionFieldArray
              name  = 'Государственные услуги'
              value = {get(action, ['Государственные услуги (функции) государственного органа', 'Государственные услуги'])}/>
            <ActionFieldArray
              name  = 'Государственные функции'
              value = {get(action, ['Государственные услуги (функции) государственного органа', 'Государственные функции'])}/>
            <ActionDocs
              name  = 'Основание для исполнения государственной функции'
              value = {get(action, ['Государственные услуги (функции) государственного органа', 'Основание для исполнения государственной функции'])}/>
          </Section>
          <Section
            info    = {size(action['Сведения о документах по информатизации']) + '/' + 2}
            padding = {null}
            title   = 'Сведения о документах по информатизации'>
            <ActionDocs
              stub
              name  = 'Документ по информатизации'
              value = {get(action, ['Сведения о документах по информатизации', 'Документ по информатизации'])}/>
            <ActionDocs
              name  = 'Обоснование мероприятия по информатизации'
              value = {get(action, ['Сведения о документах по информатизации', 'Обоснование мероприятия по информатизации'])}/>
          </Section>
          <Section
            info    = {size(action['Сведения о целевых показателях и индикаторах']) + '/' + 1}
            title   = 'Сведения о целевых показателях и индикаторах'>
            {checkType(this.props.action['Сведения о целевых показателях и индикаторах'])}
          </Section>
          <Section
            info    = {size(action['Сведения о товарах, работах, услугах']) + '/' + 3}
            title   = 'Сведения о товарах, работах, услугах'>
            {checkType(this.props.action['Сведения о товарах, работах, услугах'])}
          </Section>
          <Section
            padding = {null}
            info    = {size(action['Сведения об объемах бюджетных ассигнований']) + '/' + 4}
            title   = 'Сведения об объемах бюджетных ассигнований'>
            <ActionField
              name  = 'Объём бюджетных ассигнований на текущий год'
              value = {get(action, ['Сведения об объемах бюджетных ассигнований', 'Объём бюджетных ассигнований на текущий год'])}/>
            <ActionField
              name  = 'Объём бюджетных ассигнований на первый год планового периода'
              value = {get(action, ['Сведения об объемах бюджетных ассигнований', 'Объём бюджетных ассигнований на первый год планового периода'])}/>
            <ActionField
              name  = 'Объём бюджетных ассигнований на второй год планового периода'
              value = {get(action, ['Сведения об объемах бюджетных ассигнований', 'Объём бюджетных ассигнований на второй год планового периода'])}/>
            <ActionField
              name  = 'Совокупный объём бюджетных ассигнований'
              value = {get(action, ['Сведения об объемах бюджетных ассигнований', 'Совокупный объём бюджетных ассигнований'])}/>
          </Section>
          <Section
            padding = {null}
            info    = {size(action['Сведения о необходимости проведения оценки мероприятия по информатизации']) + '/' + 4}
            title   = 'Сведения о необходимости проведения оценки мероприятия по информатизации'>
            <ActionField
              name  = 'Вид закупки'
              value = {get(action, ['Сведения о необходимости проведения оценки мероприятия по информатизации', 'Вид закупки'])}/>
            <ActionField
              name  = 'Сведения об оценке мероприятия по информатизации'
              value = {get(action, ['Сведения о необходимости проведения оценки мероприятия по информатизации', 'Сведения об оценке мероприятия по информатизации'])}/>
            <ActionField
              name  = 'Изменение параметров мероприятия по информатизации'
              value = {get(action, ['Сведения о необходимости проведения оценки мероприятия по информатизации', 'Изменение параметров мероприятия по информатизации'])}/>
            <ActionField
              name  = 'Оценка мероприятия по информатизации'
              value = {get(action, ['Сведения о необходимости проведения оценки мероприятия по информатизации', 'Оценка мероприятия по информатизации'])}/>
          </Section>
          <Hide for = {['Куратор МКС', 'Обозреватель']}>
            <FloatingActionButton onTouchTap={this.edit} style={{position: 'fixed', right: '3rem', bottom: '3rem'}} >
              <ImageEdit />
            </FloatingActionButton>
          </Hide>
      </Container>
    )
  }
})

export default ActionView
