import React, { PropTypes } from 'react'
import axios from 'axios'
import { map, concat, last, find, filter, omit } from 'lodash'
import { List, Paper, ListItem, DropDownMenu } from 'material-ui'
import Container from '../../Components/Container'
import Flex from '../../Components/Flex'
import Hide from '../../Components/Hide'
import OGVName from '../../Components/OGVName'
import Interpunct from '../../Components/Interpunct'
import { Link } from 'react-router'

const PlanLI = (plan) =>
  <Link to={'/plans/' + plan['Идентификатор']}>
    <ListItem
      primaryText     = {(plan['Департамент'] || 'Сводный план') + ' на ' + plan['Год начала планового периода'] + ' — ' + (plan['Год начала планового периода'] + 2)}
      secondaryText   = {plan['Статус']}
      style = {plan['Департамент'] ? null : {borderBottom: '1px solid lightgray'}}
      />
  </Link>
const PlanLIWithOGV = (plan) =>
  <Link to={'/plans/' + plan['Идентификатор']}>
    <ListItem
      style={{lineHeight: '3rem'}}
      primaryText = {<div><OGVName grbs={plan['ГРБС ОГВ']} /> {plan['Год начала планового периода'] + ' — ' + (plan['Год начала планового периода'] + 2)}</div> }
      secondaryText   = {<Interpunct>{plan['Статус']}{'Сводный план'}</Interpunct>} />
  </Link>

const Plans = React.createClass({

  contextTypes: {
    router: React.PropTypes.object.isRequired,
    error:  React.PropTypes.func.isRequired,
    user:   React.PropTypes.func.isRequired
  },

  getPlans(param, value) {
    let params    = this.state.params
    params.offset = 0
    value !== null
      ? params[param] = value
      : params = omit(params, param)
    axios({
      url:    'plans',
      method: 'get',
      params: params
    }).then((response) =>
      {
      const mainPlan = find(response.data, item => item['Департамент'] === undefined)
      const plans    = this.context.user().user.role === 'Куратор МКС' || this.context.user().user.role === 'Обозреватель'
                        ? response.data
                        : filter(response.data, item => item['Департамент'] !== undefined)
      this.setState({plans, mainPlan, params, more: response.data.length >= 20})
    })
  },

  componentWillMount() {
    this.getPlans('status', this.context.user().user.role === 'Куратор МКС' ? 'На экспертизе в МКС' : null)
    axios.get('dics/dictionaries/ogv').then(response =>
      this.setState({
        filters: {
          status: {
            value: this.context.user().user.role === 'Куратор МКС' ? 'На экспертизе в МКС' : null,
            items: [{
              value:       null,
              primaryText: 'Все статусы'
            }, {
              value:       'Черновик',
              primaryText: 'Черновик'
            }, {
              value:       'На экспертизе в МКС',
              primaryText: 'На экспертизе в МКС'
            }, {
              value:       'Положительное заключение',
              primaryText: 'Положительное заключение'
            }, {
              value:       'Отрицательное заключение',
              primaryText: 'Отрицательное заключение'
            }]
          },
          grbs: {
            value: null,
            items: concat(
              [{value: null, primaryText: 'Все ОГВ'}],
              map(response.data, item => ({value: item.grbs, primaryText: item.short_name})))
          }
        }
      })
    )
  },

  getInitialState() {
    return {
      plans: [],
      params: {
        limit:  20,
        offset: 0,
        grbs:   this.context.user().user.grbs,
        isDepartment: this.context.user().user.role === 'Куратор МКС' || this.context.user().user.role === 'Обозреватель'
                        ? false
                        : null
      },
      more: true,
    }
  },

  filter(param, event, index, value) {
    let filters = this.state.filters
    filters[param].value = value
    this.setState({
      filters: filters
    })
    this.getPlans(param, value)
  },

  more() {
    let params     = this.state.params
    params.offset += 20
    axios({
      url:    'plans',
      method: 'get',
      params: params
    })
    .then((response) => this.setState({plans: concat(this.state.plans, response.data), params, more: response.data.length >= 20}))
  },

  render() {
    return (
      <Container width='70rem'>
        <Hide
          style = {{margin: '-1rem 0'}}
          for   = {['Руководитель департамента', 'Куратор ОГВ']}>
        {this.state.filters
          ? <Flex itemCenter style = {{marginTop: '-4rem', padding: '1rem 0'}}>
              <DropDownMenu
                style              = {{marginTop:    '-7px', flexShrink: 0}}
                labelStyle         = {{color:        'gray',
                                      textTransform: 'uppercase',
                                      fontSize:      '13px'}}
                underlineStyle     = {{display:      'none'}}
                value              = {this.state.filters.status.value}
                onChange           = {this.filter.bind(this, 'status')}>
                {map(this.state.filters.status.items, item =>
                  <ListItem
                    style          = {{lineHeight: '3rem'}}
                    innerDivStyle  = {{padding: '1rem 3rem' }}
                    key            = {item.value}
                    value          = {item.value}
                    primaryText    = {item.primaryText} />)}
              </DropDownMenu>
              <div>
                <DropDownMenu
                  autoWidth          = {false}
                  menuStyle          = {{width: '80rem'}}
                  style              = {{flexShrink: 0, maxWidth: '50rem', overflow: 'hidden'}}
                  labelStyle         = {{color: 'gray', textTransform: 'uppercase', fontSize: '13px'}}
                  underlineStyle     = {{display: 'none'}}
                  value              = {this.state.filters.grbs.value}
                  onChange           = {this.filter.bind(this, 'grbs')}>
                  {map(this.state.filters.grbs.items, item =>
                    <ListItem
                      style          = {{lineHeight: '3rem'}}
                      innerDivStyle  = {{padding: '1rem 3rem' }}
                      key            = {item.value}
                      value          = {item.value}
                      primaryText    = {item.primaryText} />)}
                </DropDownMenu>
                </div>
            </Flex>
          : null}
          </Hide>
        <Paper style={{position: 'relative', marginBottom: '6rem'}}>
          {this.state.plans.length > 0
            ? <List>
                {this.state.mainPlan && ['Куратор МКС', 'Обозреватель'].indexOf(this.context.user().user.role) >= 0
                  ? null
                  : <PlanLI {...this.state.mainPlan}/>}
                {map(this.state.plans, (plan) =>
                  ['Куратор МКС', 'Обозреватель'].indexOf(this.context.user().user.role) >= 0
                    ? <PlanLIWithOGV {...plan} key={plan.id}/>
                    : <PlanLI {...plan} key={plan.id}/>)}
                {this.state.more
                  ? <ListItem style={{
                      marginBottom:  '-1rem',
                      width:         '100%',
                      padding:       '1rem',
                      color:         '#333',
                      textTransform: 'uppercase',
                      borderTop:     '1px solid rgba(0,0,0,.1)'}}
                      onTouchTap = {this.more}>
                      Загрузить ещё
                    </ListItem>
                  : null}
              </List>
              : <div style={{padding: '3rem'}}>Нет планов по заданным параметрам</div>}
        </Paper>
      </Container>
    )
  }
})

export default Plans
