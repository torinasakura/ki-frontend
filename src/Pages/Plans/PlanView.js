import React, { Component } from 'react'
import { Paper } from 'material-ui'
import { Link } from 'react-router'
import { map, get, size, concat } from 'lodash'
import axios from 'axios'
import Section from '../../Components/Section'
import OGVName from '../../Components/OGVName'
import ActionLI from '../Actions/ActionLI'

const PlanView = React.createClass({

  contextTypes: {
    router:     React.PropTypes.object.isRequired,
    error:      React.PropTypes.func.isRequired,
    user:       React.PropTypes.func
  },

  componentWillMount() {
    axios({
      method:  'get',
      url:     'plans/' + this.props.params.slug
    }).then( (response) =>
      response.data.error
        ? this.context.error(response.data.error)
        : (this.setState({plan: response.data}),
           this.getActions(response.data['Год начала планового периода']))
    )
  },

  getActions(year) {
    axios({
      method:  'get',
      url:     'events',
      params: {
        limit: 1000,
        grbs:  this.state.plan['ГРБС ОГВ'],
        dept:  this.state.plan['Департамент'] || null,
        year:  year
      }
    }).then( (response) =>
      response.data.error
        ? this.context.error(response.data.error)
        : this.setState({actions: response.data})
    )
  },

  getInitialState() {
    return {
      plan:    {},
      actions: []
    }
  },

  render() {
    return (
      <div>
        {this.state.plan
          ? <Paper style={{padding:'3rem'}}>
            <h1>План <span className='plan__number'>{`${this.state.plan['Год начала планового периода']} - ${this.state.plan['Год начала планового периода'] + 2}`}</span></h1>
              <div>
                <OGVName grbs={this.state.plan['ГРБС ОГВ']} style={{flexShirnk:0}} /> · {this.state.plan['Департамент'] || 'Сводный план ОГВ (по всем департаментам)'}
              </div>
            </Paper>
          : null}
          {this.state.plan['Показатели']
            ? <Section title='Показатели'>
                <table className='plan__costs'>
                  <thead>
                    <tr>
                      <th>Наименование показателя</th>
                      <th>Базовое значение</th>
                      <th>2017</th>
                      <th>2018</th>
                      <th>2019</th>
                    </tr>
                  </thead>
                  <tbody>
                    {map(this.state.plan['Показатели'], (rate, i) =>
                      <tr key={i}>
                        <td>{rate['Наименование показателя']}</td>
                        <td>{rate['Базовое значение']}</td>
                        <td>{rate['Плановое значение на первый год планового периода']}</td>
                        <td>{rate['Плановое значение на второй год планового периода']}</td>
                        <td>{rate['Плановое значение на третий год планового периода']}</td>
                      </tr>)}
                  </tbody>
                </table>
              </Section>
            : null}
          {this.state.plan['Индикаторы']
            ? <Section title='Индикаторы'>
            <table className='plan__costs'>
              <thead>
                <tr>
                  <th>Наименование Индикатора</th>
                  <th>Базовое значение</th>
                  <th>2017</th>
                  <th>2018</th>
                  <th>2019</th>
                </tr>
              </thead>
              <tbody>
                {map(this.state.plan['Индикаторы'], (rate, i) =>
                  <tr key={i}>
                    <td>{rate['Наименование индикатора']}</td>
                    <td>{rate['Базовое значение']}</td>
                    <td>{rate['Плановое значение на первый год планового периода']}</td>
                    <td>{rate['Плановое значение на второй год планового периода']}</td>
                    <td>{rate['Плановое значение на третий год планового периода']}</td>
                  </tr>
                )}</tbody>
              </table>
          </Section> : null}
            <h2 style={{padding:'3rem', color:'rgba(0,0,0,.64)'}}>
              {size(this.state.actions)} мероприятий
            </h2>
          <Paper>
              {this.state.actions.length > 0
                ? map(this.state.actions, (action, key) =>
                  <ActionLI {...action} key={key} update={this.getActions.bind(null, this.state.plan['Год начала планового периода'])}/>)
                : <div style={{padding:'2rem'}}>
                    В плане информатизации отсутствуют мероприятия. Вы можете добавить их из списка мероприятий вашего департамента\ОГВ или создать вновь.
                    <br/>
                    <br/>
                  </div>}
          </Paper>
      </div>
    )
  }
})

export default PlanView
