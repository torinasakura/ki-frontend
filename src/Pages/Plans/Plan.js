import React from 'react'
import axios from 'axios'
import { map, concat } from 'lodash'
import Menu from '../../Components/Menu'
import { DropDownMenu, MenuItem, Divider, IconButton } from 'material-ui'
import FileFolder from 'material-ui/svg-icons/file/folder'
import { Link } from 'react-router'
import Container from '../../Components/Container'

const Plan = React.createClass({

  contextTypes: {
    router: React.PropTypes.object.isRequired,
    error:  React.PropTypes.func.isRequired,
    user:   React.PropTypes.func.isRequired,
  },

  fetchPlan() {
    axios({
      method:  'get',
      url:     'plans/' + this.props.params.slug
    }).then((response) =>
      response.data.error
        ? this.context.error(response.data.error)
        : this.setState({data: response.data}))
  },

  componentWillMount() {
    this.fetchPlan()
    axios({
      method: 'get',
      url:    `status/${this.props.params.slug}`,
      params: {
        role: this.context.user().user.role
      }
    }).then((response) =>
      this.setState({
        status:      response.data.status,
        transitions: concat([], response.data.transitions, response.data.status)
      })
    )
  },

  changeStatus(event, i, nextStatus) {
    if (nextStatus !== 'history')
      axios.patch(`/plans/${this.props.params.slug}/change_status`, {
        status: nextStatus
      }).then((response) => response.data.error
        ? this.context.error(response.data.error)
        : this.setState({
            status:      response.data.status,
            transitions: concat([], response.data.transitions, response.data.status)
          }))
  },

  render() {
    return (
      <div>
        <Menu back = 'plans'>
          <Link to = {`/plans/${this.props.params.slug}`}>План</Link>
          <DropDownMenu
                labelStyle     = {{color: 'white'}}
                underlineStyle = {{display: 'none'}}
                value          = {this.state ? this.state.status : 'Статус'}
                onChange       = {this.changeStatus}
                style          = {{height: 'auto'}}>
                {this.state
                  ? map(this.state.transitions, (transition) =>
                    <MenuItem
                      key         = {transition}
                      value       = {transition}
                      primaryText = {transition}/>)
                  : <MenuItem primarнText='Статус' value='Статус'/>}
              <Divider />
              <MenuItem value='history' primaryText ='История статусов' disabled/>
            </DropDownMenu>
            <Link to = {`/plans/${this.props.params.slug}/files`}>
              <IconButton tooltip='Файлы'>
                <FileFolder color = 'white' />
              </IconButton>
            </Link>
        </Menu>
          <Container>
            {this.state
              ? this.props.children && React.cloneElement(this.props.children, {
                  plan:   this.state.data,
                  update: this.fetchPlan})
              : null}
          </Container>
      </div>
    )
  }
})

export default Plan
