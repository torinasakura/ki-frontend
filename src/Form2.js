import React, { PropTypes } from 'react'
import { forEach, isArray, isObject, isFunction, isBoolean, map, size, reduce, assign, remove, isNil, get, concat, uniqBy } from 'lodash'
import update from 'react-addons-update'
import { TextField, SelectField, MenuItem, Toggle, FlatButton, Avatar, DatePicker, LinearProgress, IconButton } from 'material-ui'
import FileFileDownload from 'material-ui/svg-icons/file/file-download'
import NavigationClose from 'material-ui/svg-icons/navigation/close'
import Select from 'react-select'
import axios from 'axios'
import moment from 'moment'
import areIntlLocalesSupported from 'intl-locales-supported'

let DateTimeFormat
if (areIntlLocalesSupported(['ru'])) {
  DateTimeFormat = global.Intl.DateTimeFormat
} else {
  const IntlPolyfill = require('intl')
  DateTimeFormat = IntlPolyfill.DateTimeFormat
  require('intl/locale-data/jsonp/ru')
}

const MyDate = React.createClass({
  returns(event, value){
    this.props.returns(value)
  },
  render() {
    return(
      <DatePicker
        fullWidth
        autoOK
        floatingLabelText = {this.props.label}
        value             = {this.props.value ? new Date(this.props.value) : null}
        locale            = 'ru'
        okLabel           = 'ОК'
        cancelLabel       = 'Отменить'
        DateTimeFormat    = {DateTimeFormat}
        formatDate        = {(date) => moment(date).format('LL')}
        name              = {this.props.name}
        onChange          = {this.returns}
      />
    )
  }
})

const File = React.createClass({

  componentWillMount() {
    if (this.props.value)
      axios.get('files/' + this.props.value + '/info')
      .then((response) => this.setState({
        file: {
          name: response.data.fileName}}))
  },

  download() {
    window.open(axios.defaults.baseURL + '/files/' + this.props.value)
  },

  remove() {
    this.props.returns(null)
    this.setState({
      file: null
    })
  },

  input() {
    if (this.refs.file.files[0]) {
      this.setState({
        file:      this.refs.file.files[0],
        error:     null,
        uploading: true
      })
      const formData = new FormData()
      formData.append('file', this.refs.file.files[0])
      var request = new XMLHttpRequest()
      request.onreadystatechange = () => {
        if (request.status == 201) {
          this.setState({uploading: false})
          this.props.returns(JSON.parse(request.responseText).uuid)
        } else if (request.status != 201 && request.status != 0) {
          this.setState({
            uploading: false,
            error:     'Ошибка (' + request.statusText + ')'
          })
        }
      }
      request.open("POST", "http://62.205.160.6:15080/api/files")
      request.setRequestHeader('Authorization', 'Token ' + JSON.parse(localStorage.getItem('user')).token)
      request.send(formData)
      // axios({
      //   method: 'post',
      //   url:    'files',
      //   data:   this.refs.file.files[0],
      //   headers: {
      //     'Content-Type': 'multipart/form-data'
      //   }
      // })
      // .then((response) => {
      //   this.setState({uploading: false})
      //   this.props.returns(response.data.uuid)
      // })
      // .catch((error) => this.setState({
      //   uploading: false,
      //   error: 'Ошибка'}))
    } else {
      this.setState({
        uploading: false,
        error:     null
      })
    }
  },

  getInitialState() {
    return {
      uploading: false
    }
  },

  render() {
    const controls = () => {if (this.props.value) {
      return (
      <div style={{position:'absolute', right:'-1rem', bottom:'.5rem'}}>
        <IconButton
          style     = {{width:'3rem', height:'3rem', padding:'.5rem'}}
          iconStyle = {{width:'1.9rem', height:'1.9rem'}}
          onClick   = {this.download}>
          <FileFileDownload color='gray'/>
        </IconButton>
        <IconButton
          style     = {{width:'3rem', height:'3rem', padding:'.5rem'}}
          iconStyle = {{width:'2rem', height:'2rem'}}
          onClick   = {this.remove}>
          <NavigationClose color='gray'/>
        </IconButton>
      </div>
    )
  }}
    return (
      <div style={{position:'relative'}}>
        <TextField
          errorText         = {this.state.error}
          name              = {this.props.name}
          floatingLabelText = {this.props.label}
          value             = {get(this.state, 'file.name')}
          fullWidth/>
          {this.state.uploading
            ? <LinearProgress
                style={{position:'absolute', marginTop:'-1rem'}}
                mode='indeterminate'/> : null}
          <input
            type='file' ref='file' onChange={this.input}
            style={{opacity:0, left:0, width: '100%', cursor:'pointer', position:'absolute', bottom:0, top: 0}}/>
          {controls()}
      </div>
    )
  }
})

const ArrayOfObjects = React.createClass({

  propTypes: {
    value: React.PropTypes.array,
  },

  getDefaultProps() {
    return {
      value: []
    }
  },

  returnValues(values, path) {
    let newValues   = this.props.value.slice(0)
    this.props.object.length === 1 ? newValues[path] = map(values, (value) => value)[0] :
    newValues[path] = values
    this.props.returns(newValues)
  },

  add() {
    if (this.props.object.length > 1) {
      let newObject = {}
      forEach(this.props.object, (object) => newObject[object.name] = null)
      this.props.returns(this.props.value.concat(newObject))
    } else {
      this.props.returns(this.props.value.concat(''))}
  },

  returnValue(path, value) {
      let newValues   = this.props.value.slice(0)
      newValues[path] = value
      this.props.returns(newValues)
  },

  remove(i) {
    let newValues = this.props.value.slice(0)
    newValues.splice(i, 1)
    this.props.returns(newValues)
  },

  render() {
    const array = this.props.object.length > 1
    return(
      <div className='array'>
        <div className='array__title'>{this.props.label}</div>
          {map(this.props.value, (obj, i) =>
          <div key={i} className='array__item'>
              <Form
                data     = {this.props.object}
                values   = {array ? obj : {[this.props.object[0].name]: obj}}
                returns  = {this.returnValues}
                moreData = {this.props.moreData}
                i        = {i}/>
            <div className='array__item__separator' onClick={this.remove.bind(null, i)}>
              <div className='array__item__separator__remove'>
                <div className='array__item__separator__remove__cross'>+</div>
              </div>
            </div>
          </div>)}
      {this.props.addable ?
          <FlatButton onClick={this.add} style={{marginLeft:'-2rem', marginTop:'2rem'}} label='Добавить'/> : null}
    </div>
    )
  }
})

// const Text = (block) =>
//   <TextField
//     floatingLabelText = {block.label}
//     name              = {block.name}
//     type              = {block.type}
//     value             = {block.value}
//     onChange          = {block.returns}
//     errorText         = {block.error}
//     disabled          = {block.disabled}
//     multiLine         = {block.multiline}
//     fullWidth />

const Text = React.createClass({

  return(e) {
    const value = e.target.value
    this.props.returns(value)
  },

  render() {
    return (
      <TextField
        style             = {{overflow:'hidden', display:'block'}}
        floatingLabelText = {this.props.label}
        name              = {this.props.name}
        type              = {this.props.type}
        value             = {this.props.value || ''}
        onChange          = {this.return}
        errorText         = {this.props.error}
        disabled          = {this.props.disabled}
        multiLine         = {this.props.multiline}
        fullWidth />
    )
  }
})


const convert = (options) =>
  options ?
  isObject(options[0]) == false ?
    map(options, (option) => ({label: option, value: option})) :
      options :
  null

const MySelect = React.createClass({

  handleChange(event, index, value) {
    this.props.returns(value)
  },

  getDefaultProps: function() {
    return {
      options: []
    }
  },

  render() {
    return (
      <SelectField
        style             = {{overflow:'hidden', display:'block'}}
        floatingLabelText = {this.props.label}
        value             = {this.props.value}
        onChange          = {this.handleChange}
        disabled          = {this.props.disabled}
        fullWidth>
        {map(this.props.value ? uniqBy(concat(this.props.options, this.props.value)) : this.props.options, (option, i) =>
          <MenuItem
            value       = {option}
            primaryText = {<div>{this.props.optionLabel ? eval(this.props.optionLabel) : option}</div>}
            label       = {<div>{this.props.optionLabel ? eval(this.props.optionLabel) : option}</div>}
            key         = {i}/>
        )}
      </SelectField>
    )
  }
})


const MyToggle = React.createClass({
  toggle(e) {
    this.props.returns(!this.props.value)
  },
  render() {
    return (
      <Toggle
        label    = {this.props.label}
        toggled = {this.props.value}
        onToggle = {this.toggle}
        style    = {{paddingBottom: '.5rem', marginBottom:'1rem', marginTop:'4rem'}}/>
    )
  }
})

const components = {
  text:   Text,
  select: MySelect,
  date:   MyDate,
  // password:       Text,
  // number:         Text,
  toggle: MyToggle,
  array:  ArrayOfObjects,
  file:   File,
  // object:         SelectableObject
}

const Block = React.createClass({
  propTypes: {
    moreData: React.PropTypes.object.isRequired,
    block:    React.PropTypes.shape({
      name:        React.PropTypes.string.isRequired,
      type:        PropTypes.oneOf(map(components, (item, key) => key)).isRequired,
      description: React.PropTypes.string
    }),
  },

  getInitialState() {
    return {
      data: null
    }
  },

  componentWillMount() {
    this.compute(this.props)
  },

  componentWillReceiveProps(nextProps) {
    this.compute(nextProps)
  },

  compute(props) {
    let computedData = {...props.block}
    computedData.label ? null : computedData.label = computedData.name

    //Counter for async functions in block values
    let iteration = null

    //Update state with executed block attributes if exection ended
    const finalize = () => {
      if (iteration === size(computedData)) {
        this.setState({
          data: computedData
        })}
    }
    //Block function values exection
    forEach(computedData, (value, key) => {

      isFunction(value)
          //Check if async
          ? (
          // console.log('funcpropsUpdated'),
          value(computedData, props.form, props.moreData).then
            //Execute async
            ? value(computedData, props.form, props.moreData).then((result) => {
                computedData[key] = result
                iteration++
                finalize()
              })
            //Execute sync
            : (
              computedData[key] = value(computedData, props.form, props.moreData),
              iteration++,
            )
        //If not a function - go on
      ) : (
        // console.log('propsUpdated'),
        iteration++)
      finalize()
    })
  },

  returns(typedValue) {
    if (typedValue === '') {
    } else if (!isBoolean(typedValue) && isNaN(Number(typedValue)) === false && isArray(typedValue) === false) {
      typedValue = Number(typedValue)
    }
    this.props.returns(this.props.i, typedValue)
  },

  render() {
    const Component = components[this.props.block.type]

    return (
      <div className = 'block'>
        {this.state.data ? <Component
          {...this.state.data}
          returns  = {this.returns}
          moreData = {this.props.moreData}/> : <div style={{height:'72px'}}></div>}
      </div>
    )
  }
})


const Form = React.createClass({
  propTypes: {
    data:     React.PropTypes.array.isRequired,
    moreData: React.PropTypes.object,
    values:   React.PropTypes.object,
    returns:  React.PropTypes.func,
    style:    React.PropTypes.object
  },

  getInitialState() {
    return {
      form: this.props.values
        ? map(this.props.data, (field) => ({...field, value: isNil(this.props.values[field.name])
          ? field.value
          : this.props.values[field.name]}))
        : this.props.data
    }
  },

  changeValue(i, value) {
    let nextForm      = this.state.form.slice(0)
    nextForm[i].value = value

    let values = {}
    forEach(nextForm, (item) => item.value ? values[item.name] = item.value : null)
    this.props.returns ? this.props.returns(values, this.props.i) : null
    this.setState({
      form: nextForm,
      values: values
    })
  },

  render () {
    return (
      <div style={this.props.style}>
        {map(this.state.form, (block, i) =>
          <Block
            returns  = {this.changeValue}
            form     = {this.state.form}
            i        = {i}
            block    = {block}
            key      = {block.name}
            moreData = {this.props.moreData}/>)}
      </div>
    )
  }
})

export default Form
