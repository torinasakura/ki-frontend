import React from 'react'
import App from '../containers/App'
import Login from '../pages/login/containers/Login'

import Actions from '../pages/Actions/Actions'
import Action from '../pages/Actions/Action'
import Plans from '../pages/Plans/Plans'
import Plan from '../pages/Plans/Plan'
import PlanView from '../pages/Plans/PlanView'
import PlanFiles from '../pages/Plans/PlanFiles'
import NewForm from '../NewForm'
import ActionFiles from '../pages/Actions/ActionFiles'
import ActionView from '../pages/Actions/ActionView'
import ActionEdit from '../pages/Actions/ActionEdit'
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router'

const routes = (
  <Route path='/'>
    <Route component={App}>
      <Route path   = 'actions' component={Actions} title='Мероприятия'/>
      <Route path   = 'actions/:slug' component={Action}>
        <IndexRoute component={ActionView}/>
        <Route path = 'edit' component={ActionEdit}/>
        {/*<Route path = 'statuses' component={Action}/>*/}
        <Route path = 'files' component={ActionFiles}/>
      </Route>
      <Route path = 'plans' component={Plans} title='Планы'/>
      <Route path = 'NewForm' component={NewForm} title='Планы'/>
      <Route path = 'plans/:slug' component={Plan}>
        <IndexRoute component = {PlanView}/>
        <Route path = 'files' component={PlanFiles} />
      </Route>
      <IndexRoute component={Actions}/>
    </Route>
    <Route path   = 'login' component={Login} title='Логин'/>
  </Route>
)

export default routes
