import { createReducer } from '../../../utils'
import * as actions from '../constants'

const initialState = {
  login: '',
  password: '',
  error: '',
}

export default createReducer(initialState, {
  [actions.change]: (state, { value }) => ({ ...state, ...value }),
  [actions.setError]: (state, { error }) => ({ ...state, error }),
})
