import { connect } from 'react-redux'
import { change, login } from '../actions'
import Login from '../components/Login'

export default connect(
  state => ({
    login: state.login.login,
    password: state.login.password,
    error: state.login.error,
  }),
  dispatch => ({
    onChange: value => dispatch(change(value)),
    onLogin: () => dispatch(login()),
  })
)(Login)
