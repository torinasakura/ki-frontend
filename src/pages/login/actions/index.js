import axios from 'axios'
import { auth } from '../../../actions/user'
import * as actions from '../constants'

export function change(value) {
  return {
    type: actions.change,
    value,
  }
}

export function login() {
  return async (dispatch, getState) => {
    const { login, password } = getState().login

    const { data } = await axios.post('auth/login', { login, password })

    if (data.error) {
      dispatch({
        type: actions.setError,
        error: data.error,
      })
    } else {
      dispatch(auth(data))
    }
  }
}
