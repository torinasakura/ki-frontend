import React from 'react'
import { RaisedButton } from 'material-ui'
import Form from '../../../Form'

const Login = ({ login, password, error, onChange, onLogin }) => (
  <div>
    <h1>ФГИС КИ</h1>
    <Form
      data={[{
        name:  'login',
        type:  'text',
        placeholder: 'логин',
        value: login,
      }, {
        name:  'password',
        type:  'password',
        placeholder: 'пароль',
        value: password,
      }]}
      returnValues={onChange}
    />
    {error}
    <br/>
    <RaisedButton
      label='войти'
      onClick={onLogin}
    />
  </div>
)

export default Login
