import React, { PropTypes } from 'react'

const Container = (props) =>
    <div style={{
        maxWidth: props.width || '100rem',
        margin: '0 auto'}}
        {...props}>
    {props.children}
    </div>

export default Container
