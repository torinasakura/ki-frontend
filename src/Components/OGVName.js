import React, { PropTypes } from 'react'
import axios from 'axios'

const OGVName = React.createClass({

  componentWillMount() {
    this.getOGV(this.props.grbs)
  },

  componentWillReceiveProps(nextProps) {
    nextProps && nextProps.grbs !== this.props.grbs
      ? this.getOGV(nextProps.grbs)
      : null
  },

  getOGV(grbs) {
      grbs
      ? axios.get('dics/dictionaries/ogv?grbs=' + grbs).then((response) =>
        this.setState({
          ogv: response.data[0]
        })
      )
    : null
  },

  render () {
      return (
      <span>
        {this.state ? this.state.ogv.short_name : this.props.grbs}
      </span>
    )
  }
})

export default OGVName
