import React, { PropTypes } from 'react'
import { Paper, Tabs, Tab, Avatar, IconButton, FlatButton, Popover } from 'material-ui'
import NavigationArrowBack from 'material-ui/svg-icons/navigation/arrow-back'
import NavigationMenu from 'material-ui/svg-icons/navigation/menu'
import OGVName from './OGVName'
import { Link } from 'react-router'
import { indigo500 } from 'material-ui/styles/colors'
import { isString } from 'lodash'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  tabs: {
    flexGrow: 1,
    display: 'flex',
    marginLeft: '-1rem',
  },
  tab: {
    display: 'block',
    padding: '1.5rem 3rem',
    boxSizing: 'border-box',
    height: '6rem',
    color: 'rgba(255,255,255,.3)', //#C5CAE9
    fontSize: '2rem',
  },
  tabActive: {
    color: 'white',
    paddingTop: '1.5rem',
    borderBottom: '2px solid red',
  },
  userPopover: {
    paddingTop: '2rem',
    marginTop: '-2rem',
  },
  userHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: '2rem',
    cursor: 'pointer',
  },
  userInfo: {
    marginTop: '1.5rem',
    borderTop: '1px solid lightgray',
    fontSize: '13px',
    padding: '2rem',
    boxSizing: 'border-box',
    '& dl': {
      margin: 0,
    },
    '& dt': {
      width: '14rem',
      float: 'left',
    },
    '& dd': {
      display: 'block',
      width: '32rem',
      marginLeft: '14rem',
    },
  },
  userControls: {
    padding: '1rem',
    borderTop: '1px solid lightgray',
    textAlign: 'right',
  },
})

const User = React.createClass({

  contextTypes: {
    logout: React.PropTypes.func.isRequired,
  },

  getInitialState() {
    return {
      open: false
    }
  },

  toggle(event) {
    this.setState({
      open:     !this.state.open,
      anchorEl: event.currentTarget,
    })
  },

  logout() {
    this.context.logout()
  },

  render() {
    return (
      <div>
        <div onClick={this.toggle} className={ styles({ userHeader: true }) }>
          {this.props.login}
          <Avatar
            size  = {32}
            style = {{margin:'0 2rem'}}
            src   = 'https://tracker.moodle.org/secure/attachment/30912/f3.png'/>
        </div>
        <Popover
          open           = {this.state.open}
          anchorEl       = {this.state.anchorEl}
          anchorOrigin   = {{horizontal: 'right', vertical: 'top'}}
          targetOrigin   = {{horizontal: 'right', vertical: 'top'}}
          className      = { styles({ userPopover: true }) }
          onRequestClose = {this.toggle}>
          <div className = { styles({ userHeader: true }) }>
            {this.props.login}
            <Avatar size={32} style={{margin:'0 2rem'}} src='https://tracker.moodle.org/secure/attachment/30912/f3.png'/>
          </div>
            <div className={ styles({ userInfo: true }) } >
              <dl>
                <dt>ФИО</dt><dd>{`${this.props.name} ${this.props.middlename} ${this.props.surname}`}</dd>
                <dt>Роль</dt><dd>{this.props.role}</dd>
                <dt>ОГВ</dt><dd><OGVName grbs={this.props.grbs} /></dd>
                <dt>Департамент</dt><dd>{this.props.dept}</dd>
              </dl>
          </div>
          <div className={ styles({ userControls: true }) }>
            <FlatButton onTouchTap={this.logout} label='Выход'/>
          </div>
        </Popover>
      </div>
    )
  }
})

const Menu = React.createClass({

  contextTypes: {
    router:     React.PropTypes.object.isRequired,
    error:      React.PropTypes.func.isRequired,
    user:       React.PropTypes.func,
    location:   React.PropTypes.func.isRequired
  },

  back() {
    this.context.router.push(isString(this.props.back) ? '/' + this.props.back : '/')
  },

  render () {
    const user = this.context.user().user
    const active = (current) =>
      this.context.location().pathname === current
        ? styles({ tab: true, tabActive: true })
        : styles({ tab: true })

    return (
      <Paper
        style     = {{
          display:    'flex',
          alignItems: 'center',
          background: indigo500,
          position:   'fixed',
          height:     '6rem',
          top:        0,
          width:      '100%',
          zIndex:     '100',
          color:      'white'}}>
        {this.props.back
          ? <IconButton tooltip = "назад" touch={true} tooltipPosition="bottom-right" onClick={this.back} style={{margin:'0 1rem'}}>
              <NavigationArrowBack color='white'/>
            </IconButton>
          : null}
        <div style = {{flexGrow: '1', display: 'flex', alignItems: 'center'}}>
          {this.props.children}
        </div>
        <User {...user} />
      </Paper>
    )
  }
})

export default Menu
