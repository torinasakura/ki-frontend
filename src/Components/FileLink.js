import React, { PropTypes } from 'react'
import axios from 'axios'

const FileLink = React.createClass({

  componentWillMount() {
    axios.get('files/info/' + this.props.uuid)
    .then((response) => this.setState({
      file: response.data}))
  },

  getInitialState() {
    return {
      file: null
    }
  },

  download() {
    window.open(axios.defaults.baseURL + '/files/' + this.props.uuid)
  },

  render() {
    if (this.state.file) {
      return (
        <a href='#' onClick={this.download} style={{borderBottom:'1px solid lightgray', color:'#90A4AE'}}>
          {this.state.file.file_name}
        </a>
      )}
    else{ return null}
  }
})

export default FileLink
