import React from 'react'
import accounting from 'accounting'

const Money = (props) => {
  const money = accounting.formatMoney(props.sum, {
    symbol:    '₽',
    format:    '%v %s',
    thousand:  ' ',
    precision: 0 })
  return <span style={props.style}>{money}</span>
}

export default Money
