import React from 'react'
import { omit } from 'lodash'

const Hide = (props, context) =>
  props.for.indexOf(context.user().user.role) > -1 || props.if
    ? null
    : <div {...omit(props, 'children', 'for', 'if')}>{props.children}</div>

Hide.propTypes = {
  for: React.PropTypes.array.isRequired,
  if:  React.PropTypes.bool
}

Hide.contextTypes = {
  user: React.PropTypes.func.isRequired
}

export default Hide
