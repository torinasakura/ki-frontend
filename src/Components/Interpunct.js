import React from 'react'
import {map, merge} from 'lodash'

const Dot = () =>
  <span style={{margin:'0 1rem'}}>·</span>

const Interpunct = (props) =>
  <div style = {merge({display:'inline'}, props.style)}>
    {map(props.children, (child, i) => [child, i < props.children.length - 1 ? <Dot/> : null])}
  </div>

export default Interpunct
