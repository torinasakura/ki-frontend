import React, { PropTypes } from 'react'
import { Paper } from 'material-ui'
import EnhancedButton from 'material-ui/internal/EnhancedButton'

const Section = React.createClass({

  getDefaultProps() {
    return {
      expanded: false,
      padding:  '0 3rem 3rem 3rem'
    }
  },

  getInitialState() {
    return {
      expanded: this.props.expanded,
    }
  },

  expand(){
    this.setState({
      expanded: !this.state.expanded
    })
  },

  render() {
    return (
      <Paper
        rounded = {this.state.expanded ? true : false}
        style   = {{margin: this.state.expanded ? '2rem 0' : 0}}>
        <EnhancedButton
          containerElement = 'div'
          style            = {{display:'block'}}
          onTouchTap       = {this.expand}>
          <div
            style = {{
              display: 'flex',
              padding: this.state.expanded ? '3rem' : '1.5rem 3rem'}}>
            <h2
              style = {{
                flexGrow:   '1',
                fontSize:   this.state.expanded ? '1.618em' : '14px',
                fontWeight: this.state.expanded ? 500 : 400,
                maxHeight:  this.state.expanded ? '9rem' : '3rem',
                color:      'rgba(0,0,0,.72)',
                overflow:   'hidden'}}>
                {this.props.title}</h2>
              <div style = {{color:'gray', paddingLeft:'2rem'}}>{this.props.info}</div>
          </div>
        </EnhancedButton>
        {this.state.expanded
          ? <div style={{padding: this.props.padding}}>
            {this.props.children}</div>
          : null}
      </Paper>
    )
  }
})

export default Section
