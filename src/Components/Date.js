import React, { PropTypes } from 'react'
import moment from 'moment'
import Tooltip from 'material-ui/internal/Tooltip'

const HumanDate = React.createClass({

  getInitialState() {
    return {
      hovered: false
    }
  },

  hover(){
    this.setState({
      hovered: !this.state.hovered
    })
  },
  render () {
    return (
      <div
        onMouseEnter = {this.hover}
        onMouseLeave = {this.hover}
        style        = {{position: 'relative', display:'inline-block'}}>
        {moment(this.props.value).format(
          this.props.format ||
          moment(this.props.value).get('year') === moment().get('year') ? 'D MMMM' :'LL')}
        <Tooltip
          show               = {this.state.hovered}
          label              = {moment(this.props.value).format('D MMMM YYYY')}
          horizontalPosition = 'left'
          touch              = {true} />
        </div>
    )
  }
})

export default HumanDate
