import { createStore, compose, applyMiddleware } from 'redux'
import { reduxReactRouter } from 'redux-router'
import thunk from 'redux-thunk'
import persistStorage from './persistStorage'
import createHistory from './createHistory'
import rootReducer from '../reducers'

const enhancer = compose(
  reduxReactRouter({ createHistory }),
  applyMiddleware(thunk),
  persistStorage,
  window.devToolsExtension ? window.devToolsExtension() : f => f,
)

export default function configureStore(initialState) {
  const store = createStore(rootReducer, initialState, enhancer)

  store.history.listen(location => {
    if (!/^\/login/.test(location.pathname)) {
      if (!store.getState().user.token) {
        store.history.push('/login')
      }
    }
  })

  if (module.hot) {
    module.hot.accept('../reducers', () =>
      store.replaceReducer(require('../reducers').default) // eslint-disable-line global-require
    )
  }

  return store
}
