import React, { PropTypes } from 'react'
import { map, forEach } from 'lodash'
import Form from './Form.js'
import axios from 'axios'
import { RaisedButton } from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
const theme = getMuiTheme(lightBaseTheme)

const Login = React.createClass({
  contextTypes:{
    login: React.PropTypes.func.isRequired,
    error: React.PropTypes.func.isRequired,
    router: React.PropTypes.object.isRequired
  },
  returnValues(values) {
    // values['role'] = 'admin'
    this.setState({
      values: values,
      error: null
    })
  },

  getInitialState() {
    return {
      values: {
        user:     'tmp',
        password: 'tmp',
        // role:     'admin'
      },
      error: null,
      form: [{
        name:  'login',
        type:  'text',
        placeholder: 'логин',
        value: ''
      }, {
        name:  'password',
        type:  'password',
        placeholder: 'пароль',
        value: '',
      }]
    }
  },

  login() {
    axios.post('auth/login', this.state.values)
    .then((response) => {
      if(response.data.error) {
        this.context.error(response.data.error)
      } else {
        this.context.login(response.data)
        if (this.props.location.pathname === '/login') {
          this.context.router.push('/actions')
        }
      }
    })
  },

  render () {
    return (
      <MuiThemeProvider muiTheme={theme} >
        <div className='login'>
        <div className='login_container'>
          <h1>ФГИС КИ</h1>
          <Form data={this.state.form} returnValues={this.returnValues} />
          {this.state.error}
          <br/>
          <RaisedButton label='войти' onClick={this.login} />
        </div>
      </div>
      </MuiThemeProvider>
    )
  }
})

export default Login
