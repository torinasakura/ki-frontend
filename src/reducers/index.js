import { compose, combineReducers } from 'redux'
import { routerStateReducer as router } from 'redux-router'
import { mergePersistedState } from 'redux-localstorage'
import user from './user'
import login from '../pages/login/reducers'

const reducers = combineReducers({
  router,
  user,
  login,
})

export default compose(
  mergePersistedState()
)(reducers)
