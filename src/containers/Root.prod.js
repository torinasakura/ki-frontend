import React from 'react'
import { Provider } from 'react-redux'
import storeShape from 'react-redux/lib/utils/storeShape'
import { ReduxRouter } from 'redux-router'
import MuiThemeProvider from './MuiThemeProvider'
import routes from '../routes'

const Root = ({ store }) => (
  <Provider store={store}>
    <MuiThemeProvider>
      <ReduxRouter routes={routes} />
    </MuiThemeProvider>
  </Provider>
)

Root.propTypes = {
  store: storeShape,
}

export default Root
