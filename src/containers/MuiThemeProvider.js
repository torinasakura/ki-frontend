import React from 'react'
import BaseMuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import { indigo500, indigo700 } from 'material-ui/styles/colors'

const zIndex = {
  zIndex: {
    tooltip: 5000,
  },
  palette: {
    primary1Color: indigo500,
    primary2Color: indigo700,
  }
}

const theme = getMuiTheme(lightBaseTheme, zIndex)

const MuiThemeProvider = ({ children }) => (
  <BaseMuiThemeProvider muiTheme={theme}>
    {children}
  </BaseMuiThemeProvider>
)

export default MuiThemeProvider
