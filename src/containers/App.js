import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import Layout from '../Layout'

class App extends Component {
  static childContextTypes = {
    user:     React.PropTypes.func,
    logout:   React.PropTypes.func,
    login:    React.PropTypes.func,
    error:    React.PropTypes.func,
    location: React.PropTypes.func
  }

  getChildContext() {
    return {
      user: () => this.props.user,
      logout: () => {
        console.log('logout')
      },
      login: (user) => {
        console.log('login')
      },
      error: (error) => {
        console.log('error', error)
      },
      location: () => this.props.location
    }
  }

  render() {
    return (
      <Layout>
        {this.props.children}
      </Layout>
    )
  }
}

export default connect(
  state => ({
    user: state.user,
    location: state.router.location,
  }),
  dispatch => ({
  })
)(App)
